Hello There,
the main thing to show here is a custom Caldroid library which can specially be used to show event counts date wise and is capable to show the long events too.

The library is still not in the state to handle all the things automatically, we need to handle the color mixtures till date.

We'll be making the library to handle all the things automatically just by passing the event details.

We've created a Sample project to show the usage of library and the code to get the end result as below.

![example2.png](https://bitbucket.org/repo/xb5a8G/images/2709376948-example2.png)
![example1.png](https://bitbucket.org/repo/xb5a8G/images/2181201752-example1.png)

Example:
```java
        API api = new API(mContext, true, new AsyncTaskListener() {
            @Override
            public void onFailure(String FailureCause) {
                Log.e("Failure: ", FailureCause + "");
            }

            @Override
            public void responseExtractLogic(String response) {
                try {
                    JSONObject obj_main = new JSONObject(response);
                    Logger.e("Response" + response);
                    if (obj_main.optInt("status") == 1) {
                        setAdapter();
                        calHandler.setCustomResourceForDates(response,mRoot,iv_info);
                    } else {
                        if (obj_main.optString("message").contains("not exist")) {
                            Snackbar.make(mRoot, obj_main.optString("message").replace("Event does not exist", "No events found, please check back later for updates"), Snackbar.LENGTH_LONG).show();
                            calHandler.clearCalendar();
                            iv_info.setVisibility(View.GONE);
                        }
                    }
                } catch (Exception E) {
                    onFailure(API.MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                }
            }
        });
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, API.API_GET_EVENTS, postParameters);
```

For full code, have a look at `ET\app\src\main\java\et\plutus\com\et\fragment\EventsFragment`.

You also need to generate `CaldroidSampleCustomAdapter` and `CaldroidSampleCustomFragment` as shown in (https://github.com/roomorama/Caldroid)