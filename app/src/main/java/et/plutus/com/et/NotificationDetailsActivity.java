package et.plutus.com.et;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Ketan on 27-09-2016.
 */

public class NotificationDetailsActivity extends Activity implements View.OnClickListener {

    private TextView notification_title, notification_desc;
    private String str_title, str_desc;
    private ImageView img_share_news, img_back_news;
    private TextView title_news_header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_detail);
        str_title = getIntent().getStringExtra("Title");
        str_desc = getIntent().getStringExtra("Desc");
        initViews();
        setInfo();
    }

    @TargetApi(24)
    private void setInfo() {
        notification_title.setText(str_title);
        title_news_header.setText(str_title);
        if(BuildConfig.VERSION_CODE<24)
            notification_desc.setText(Html.fromHtml(str_desc));
        else
            notification_desc.setText(Html.fromHtml(str_desc,Html.FROM_HTML_MODE_LEGACY));
        img_back_news.setOnClickListener(this);
    }

    // Init Views which are used in the screen
    private void initViews() {
        title_news_header = (TextView) findViewById(R.id.title_news_header);
        img_back_news = (ImageView) findViewById(R.id.img_back_news);
        img_share_news = (ImageView) findViewById(R.id.img_share_news);
        notification_title = (TextView) findViewById(R.id.notification_title);
        notification_desc = (TextView) findViewById(R.id.notification_desc);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back_news:
                onBackPressed();
        }
    }
}
