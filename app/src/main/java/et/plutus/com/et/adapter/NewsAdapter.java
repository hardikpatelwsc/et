package et.plutus.com.et.adapter;

/**
 * Created by Ketan on 25-01-2016.
 */

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import et.plutus.com.et.BuildConfig;
import et.plutus.com.et.NewsDetailsActivity;
import et.plutus.com.et.R;
import et.plutus.com.et.common.CircleTransform;
import et.plutus.com.et.common.ItemClickListener;
import et.plutus.com.et.model.News;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.CustomViewHolder> {
    static String[] suffixes =
            ///    0     1     2     3     4     5     6     7     8     9
                {"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                    //    10    11    12    13    14    15    16    17    18    19
                        "th", "th", "th", "th", "th", "th", "th", "th", "th", "th",
                    //    20    21    22    23    24    25    26    27    28    29
                        "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                    //    30    31
                        "th", "st"};
    private Context mContext;
    private ArrayList<News> newsList;
    private ArrayList<News> orig;
    private String str_image = "";

    public NewsAdapter(Context context, ArrayList<News> newsList) {
        this.mContext = context;
        this.newsList = newsList;
    }

    public static String convertDateFormat(String date) {
        String Date = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat output = new SimpleDateFormat("d'## 'MMM yyyy");
            SimpleDateFormat formatDayOfMonth = new SimpleDateFormat("d");

            java.util.Date d = sdf.parse(date);
            Date = output.format(d);

            int day = Integer.parseInt(formatDayOfMonth.format(d));
            Date = Date.toUpperCase().replace("##", suffixes[day]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Date;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_news_fragment, null);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    @TargetApi(24)
    public void onBindViewHolder(CustomViewHolder customViewHolder, int position) {

        customViewHolder.txt_news_title.setText(newsList.get(position).getNews_title());
        if(BuildConfig.VERSION_CODE<24)
            customViewHolder.txt_news_desc.setText(Html.fromHtml(newsList.get(position).getNews_description()));
        else
            customViewHolder.txt_news_desc.setText(Html.fromHtml(newsList.get(position).getNews_description(),Html.FROM_HTML_MODE_LEGACY));
        customViewHolder.txt_news_date.setText(convertDateFormat(newsList.get(position).getNews_publish_date()));
        customViewHolder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                Intent news = new Intent(mContext, NewsDetailsActivity.class);
                news.putExtra("newsfeed_id", newsList.get(position).getNews_id());
                mContext.startActivity(news);
            }
        });

        if (!newsList.get(position).getNews_image().equals("")) {
            str_image = newsList.get(position).getNews_image_path();

            Picasso.with(mContext).load(str_image)
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.event_placeholder).fit().centerCrop()
                    .into(customViewHolder.img_news);
        }

    }

    @Override
    public int getItemCount() {
        return (null != newsList ? newsList.size() : 0);
    }

    public void setFilter(List<News> countryModels) {
        newsList = new ArrayList<>();
        newsList.addAll(countryModels);
        notifyDataSetChanged();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        protected ImageView img_news;
        protected TextView txt_news_title, txt_news_desc, txt_news_date;
        private ItemClickListener clickListener;
        private LinearLayout mainLL;

        public CustomViewHolder(View view) {
            super(view);
            this.mainLL = (LinearLayout) view.findViewById(R.id.mainLL);
            this.img_news = (ImageView) view.findViewById(R.id.img_news);
            this.txt_news_title = (TextView) view.findViewById(R.id.txt_news_title);
            this.txt_news_desc = (TextView) view.findViewById(R.id.txt_news_desc);
            this.txt_news_date = (TextView) view.findViewById(R.id.txt_news_date);
            itemView.setOnLongClickListener(this);
            mainLL.setOnClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getPosition(), true);
            Log.e("ProductCLickedPosition", "" + getPosition());
            Snackbar.make(v, "ProductCLickedPosition" + getPosition(), Snackbar.LENGTH_SHORT).show();
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onClick(v, getPosition(), true);
            return true;
        }

    }
}
