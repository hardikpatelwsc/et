package et.plutus.com.et.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import et.plutus.com.et.R;
import uk.co.senab.photoview.PhotoView;


public class FullScreenImagePagerAdapter extends PagerAdapter {
    LayoutInflater mLayoutInflater;
    private ArrayList<String> imageList = new ArrayList<>();
    private Context context;

    public FullScreenImagePagerAdapter(Context context, ArrayList<String> imageList) {
        this.context = context;
        this.imageList = imageList;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return imageList.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.row_imagepager, container, false);
        TextView tv_current,tv_outof;
        PhotoView imageView = (PhotoView) itemView.findViewById(R.id.imageView);
        tv_current = (TextView) itemView.findViewById(R.id.tv_current);
        tv_outof = (TextView) itemView.findViewById(R.id.tv_outof);
        final ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);


        tv_current.setText((position+1)+"");
        tv_outof.setText(imageList.size()+"");
        String imgUrl = imageList.get(position);
        Log.e("URL Received:",imageList.get(position));

        if (!TextUtils.isEmpty(imgUrl)) {
            Log.e("IF:","IF");
            progressBar.setVisibility(View.VISIBLE);
            Picasso.with(context).
                    load(imgUrl).
                    into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        } else {
            Log.e("ELSE:","ELSE");
            progressBar.setVisibility(View.GONE);
            Picasso.with(context).
                    load(R.drawable.logo).
                    into(imageView);
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
