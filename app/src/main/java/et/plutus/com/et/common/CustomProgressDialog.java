package et.plutus.com.et.common;


import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import et.plutus.com.et.R;

public class CustomProgressDialog extends Dialog
{
	AnimationDrawable frameAnimation;
	ImageView imgProgress;
	ProgressBar loading_spinner;


	public CustomProgressDialog(Context context) {
		super(context);
////		getWindow().setBackgroundDrawableResource(R.drawable.loading_spinner); //temp removed
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setContentView(R.layout.progressbar);
////		imgProgress=(ImageView)findViewById(R.id.imgProgress);
////		imgProgress.setBackgroundResource(R.drawable.progress);
//        // Typecasting the Animation Drawable
////		frameAnimation = (AnimationDrawable) imgProgress.getBackground();
////		frameAnimation.start();
//        setCancelable(false);

		getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		getWindow().setBackgroundDrawableResource(android.R.color.transparent); //temp removed
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.progressbar);
//        llMainBg = (LinearLayout) findViewById(R.id.llMainBg);
		setCancelable(false);
		loading_spinner = (ProgressBar) findViewById(R.id.loading_spinner);
		loading_spinner.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.colorPrimary)
				, PorterDuff.Mode.MULTIPLY);

	}

}