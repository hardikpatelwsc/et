package et.plutus.com.et.model;

/**
 * Created by Ketan on 21-09-2016.
 */

public class News {

    String news_id;
    String news_title;
    String news_description;
    String news_show_id;
    String category_id;
    String news_image_name;
    String news_image_path;
    String news_hits;
    String news_reference_module;
    String news_reference_module_id;
    String news_send_status;
    String news_publish_date;
    String news_publish_time;
    boolean is_active;
    boolean is_delete;
    String created_date;
    String updated_date;

    public String getNews_id() {
        return news_id;
    }

    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }

    public String getNews_title() {
        return news_title;
    }

    public void setNews_title(String news_title) {
        this.news_title = news_title;
    }

    public String getNews_description() {
        return news_description;
    }

    public void setNews_description(String news_description) {
        this.news_description = news_description;
    }

    public String getNews_show_id() {
        return news_show_id;
    }

    public String getNews_image_path() {
        return news_image_path;
    }

    public void setNews_image_path(String news_image_path) {
        this.news_image_path = news_image_path;
    }

    public void setNews_show_id(String news_show_id) {
        this.news_show_id = news_show_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getNews_image() {
        return news_image_name;
    }

    public void setNews_image(String news_image) {
        this.news_image_name = news_image;
    }

    public String getNews_hits() {
        return news_hits;
    }

    public void setNews_hits(String news_hits) {
        this.news_hits = news_hits;
    }

    public String getNews_reference_module() {
        return news_reference_module;
    }

    public void setNews_reference_module(String news_reference_module) {
        this.news_reference_module = news_reference_module;
    }

    public String getNews_reference_module_id() {
        return news_reference_module_id;
    }

    public void setNews_reference_module_id(String news_reference_module_id) {
        this.news_reference_module_id = news_reference_module_id;
    }

    public String getNews_send_status() {
        return news_send_status;
    }

    public void setNews_send_status(String news_send_status) {
        this.news_send_status = news_send_status;
    }

    public String getNews_publish_date() {
        return news_publish_date;
    }

    public void setNews_publish_date(String news_publish_date) {
        this.news_publish_date = news_publish_date;
    }

    public String getNews_publish_time() {
        return news_publish_time;
    }

    public void setNews_publish_time(String news_publish_time) {
        this.news_publish_time = news_publish_time;
    }

    public boolean is_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public boolean is_delete() {
        return is_delete;
    }

    public void setIs_delete(boolean is_delete) {
        this.is_delete = is_delete;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

}
