package et.plutus.com.et.common;

import android.graphics.drawable.GradientDrawable;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by HARDIKP on 09/27/2016.
 */

public class AppConstants {


    public static final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
    public static final SimpleDateFormat formatter_api = new SimpleDateFormat("yyyy-MM-dd");


    //
    public static String convertDateFormat_SIMPLEtoAPI(String date) {
        String formattedDate2 = "";
        try {

            SimpleDateFormat sdf = AppConstants.formatter;
            SimpleDateFormat output = AppConstants.formatter_api;

            Date d = sdf.parse(date);
            formattedDate2 = output.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate2;
    }


    public static boolean setViewColor(View view, int Color) {
        GradientDrawable gd = (GradientDrawable) view.getBackground().getCurrent();
        gd.setColor(Color);
        return true;
    }


    // Method to check if any particular date is between 2 other dates, this does not include the same date (it will return false for boundary dates)
    public static boolean dateBetween(Date toBeChecked, Date startDate, Date EndDate) {
        return startDate.compareTo(toBeChecked) * toBeChecked.compareTo(EndDate) > 0;
    }

}
