package et.plutus.com.et.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.HashMap;

import et.plutus.com.et.BuildConfig;
import et.plutus.com.et.MainActivity;
import et.plutus.com.et.R;
import et.plutus.com.et.common.Logger;
import et.plutus.com.et.webServices.API;
import et.plutus.com.et.webServices.AsyncTaskListener;
import et.plutus.com.et.webServices.JSONHelper;

/**
 * Created by Ketan on 03-05-2016.
 */
public class AboutUsFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    private TextView about_desc;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle bundle) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about_us, container, false);
        mContext = getActivity();
        initViews(view);
        getAboutUsInfo();
        return view;
    }

    void initViews(View view) {
        about_desc = (TextView) view.findViewById(R.id.about_desc);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDestroy() {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    // Getting About us info through API
    public void getAboutUsInfo() {
        //Make Post Params
        HashMap<String, String> params = new HashMap<>();
        String postParameters = JSONHelper.getJsonObjectString(params);
        ////////////////////////
        API api = new API(mContext, true, new AsyncTaskListener() {
            @Override
            public void onFailure(String FailureCause) {
                Log.e("Failure: ", FailureCause + "");
            }

            @Override
            @TargetApi(24)
            public void responseExtractLogic(String response) {
                try {
                    JSONObject obj_main = new JSONObject(response);
                    Logger.e("Response" + response);
                    JSONObject obj_data = obj_main.optJSONObject("data");
                    JSONObject obj_show = obj_data.optJSONObject("Show");
                    if(BuildConfig.VERSION_CODE<24)
                        about_desc.setText(Html.fromHtml(obj_show.optString("description")));
                    else
                        about_desc.setText(Html.fromHtml(obj_show.optString("description"),Html.FROM_HTML_MODE_LEGACY));
                } catch (Exception E) {
                    onFailure(API.MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                }
            }
        });
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, API.API_ABOUT_US, postParameters);


    }
}
