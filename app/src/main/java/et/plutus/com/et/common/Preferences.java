package et.plutus.com.et.common;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

public class Preferences {
    //pref name
    public static final String PREFS_PERMISSIONS = "prefs_permissions";
    //pref key
    public static final String PREF_PERMISSION_ASKED = "prefs_permission_asked";

    public static final String Valuepad_PREFRENCE = "Valuepad";
    public static final String VersionName = "versionName", UserID = "UserID";
    public static final String UserName = "UserName";
    public static final String isIntro = "isIntro";
    public static final String isLogin = "isLogin";
    public static final String Token = "Token";
    public static final String FCM_Token = "Fcm_Token";
    public static final String FCM_ID = "Fcm_Id";
    public static final String FCM_TYPE = "Fcm_Type";
    public static final String IMAGE_LOGO = "Image_Logo";
    public static final String FCM_Order_Id = "Fcm_Order_Id";
    public static final String FCM_Order_Action = "Fcm_Order_Action";
    public static final String FCM_REGISTERED_ID = "Fcm_Registered_Id";
    public static final String Appraiser_ID = "Appraiser_ID";
    public static final String Expired_Session = "Expired_Session";
    public static final String PAID_TotalPage = "PAID_TotalPage";
    public static final String UNPAID_TotalPage = "UNPAID_TotalPage";
    public static final String NEW_TotalPage = "NEW_TotalPage";
    public static final String ACCEPTED_TotalPage = "ACCEPTED_TotalPage";
    public static final String SCHEDULED_TotalPage = "SCHEDULED_TotalPage";
    public static final String INSPECTED_TotalPage = "INSPECTED_TotalPage";
    public static final String ONHOLD_TotalPage = "ONHOLD_TotalPage";
    public static final String COMPLETED_TotalPage = "COMPLETED_TotalPage";
    public static final String REVISION_TotalPage = "REVISION_TotalPage";
    public static final String DUE_TotalPage = "DUE_TotalPage";
    public static final String LATE_TotalPage = "LATE_TotalPage";
    public static final String ALL_TotalPage = "ALL_TotalPage";
    public static final String TECH_FEE = "Tech_Fee";
    public static final String MSG_UNREAD_COUNT = "Msg_Unread_Count";
    public static final String API_BASE_URL = "";
    public static String ISPROD_URL = "Isprod_Url";
    public static String SELECTED_API = "Selected API";
    public static String STAGING_API = "Staging API";
    public static String PRODUCTION_API = "Production API";
    public static String UserEmail = "UserEmail";
    public static String Password = "Password";
    public static String Platform = "android";
    public static String strBorrower_name = "";
    public static String strheaderTitle = "";
    public static String strFileNum = "";
    public static String strMainTitle = "";
    public static final String NotificationArrayName = "Preferences";
    // ////////////////////////////////////////////////////////////////

    public static void setPageCount(Context context,
                                    int NEW_TotalPage,
                                    int ACCEPTED_TotalPage,
                                    int SCHEDULED_TotalPage,
                                    int INSPECTED_TotalPage,
                                    int ONHOLD_TotalPage,
                                    int COMPLETED_TotalPage,
                                    int REVISION_TotalPage,
                                    int DUE_TotalPage,
                                    int LATE_TotalPage,
                                    int ALL_TotalPage) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("NEW_TotalPage", NEW_TotalPage);
        editor.putInt("ACCEPTED_TotalPage", ACCEPTED_TotalPage);
        editor.putInt("SCHEDULED_TotalPage", SCHEDULED_TotalPage);
        editor.putInt("INSPECTED_TotalPage", INSPECTED_TotalPage);
        editor.putInt("ONHOLD_TotalPage", ONHOLD_TotalPage);
        editor.putInt("COMPLETED_TotalPage", COMPLETED_TotalPage);
        editor.putInt("REVISION_TotalPage", REVISION_TotalPage);
        editor.putInt("DUE_TotalPage", DUE_TotalPage);
        editor.putInt("LATE_TotalPage", LATE_TotalPage);
        editor.putInt("ALL_TotalPage", ALL_TotalPage);
        editor.apply();
    }

    public static int getNEW_TotalPage(Context context) {
        return getValue_Integer(context, NEW_TotalPage);
    }

    public static int getACCEPTED_TotalPage(Context context) {
        return getValue_Integer(context, ACCEPTED_TotalPage);
    }

    public static int getSCHEDULED_TotalPage(Context context) {
        return getValue_Integer(context, SCHEDULED_TotalPage);
    }

    public static int getINSPECTED_TotalPage(Context context) {
        return getValue_Integer(context, INSPECTED_TotalPage);
    }

    public static int getONHOLD_TotalPage(Context context) {
        return getValue_Integer(context, ONHOLD_TotalPage);
    }

    public static int getCOMPLETED_TotalPage(Context context) {
        return getValue_Integer(context, COMPLETED_TotalPage);
    }

    public static int getREVISION_TotalPage(Context context) {
        return getValue_Integer(context, REVISION_TotalPage);
    }

    public static int getDUE_TotalPage(Context context) {
        return getValue_Integer(context, DUE_TotalPage);
    }

    public static int getLATE_TotalPage(Context context) {
        return getValue_Integer(context, LATE_TotalPage);
    }

    public static int getALL_TotalPage(Context context) {
        return getValue_Integer(context, ALL_TotalPage);
    }

    public static void setAccountPageCount(Context context,
                                           int PAID_TotalPage,
                                           int UNPAID_TotalPage
    ) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("PAID_TotalPage", PAID_TotalPage);
        editor.putInt("UNPAID_TotalPage", UNPAID_TotalPage);
        editor.apply();
    }

    public static int getPAID_TotalPage(Context context) {
        return getValue_Integer(context, PAID_TotalPage);
    }

    public static int getUNPAID_TotalPage(Context context) {
        return getValue_Integer(context, UNPAID_TotalPage);
    }

    // GET & SET STRING
    public static String getValue_String(Context context, String Key) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        return settings.getString(Key, "");
    }

    public static void setValue(Context context, String Key, String Value) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Key, Value);
        editor.apply();
    }

    // GET & SET INT
    public static int getValue_Integer(Context context, String Key) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        return settings.getInt(Key, 0);
    }

    public static void setValue(Context context, String Key, int Value) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(Key, Value);
        editor.apply();
    }

    // GET & SET FLOAT
    public static float getValue_Float(Context context, String Key) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        return settings.getFloat(Key, 0.0f);
    }

    public static void setValue(Context context, String Key, float Value) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(Key, Value);
        editor.apply();
    }

    // GET & SET LONG
    public static long getValue_Long(Context context, String Key) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        return settings.getLong(Key, 0);
    }

    public static void setValue(Context context, String Key, long Value) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(Key, Value);
        editor.apply();
    }

    // GET & SET BOOLEAN
    public static boolean getValue_Boolean(Context context, String Key,
                                           boolean Default) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        return settings.getBoolean(Key, Default);
    }

    public static void setValue(Context context, String Key, boolean Value) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(Key, Value);
        editor.apply();
    }

    // GET & SET String Array
    public static void saveArray(Context context, String[] array,
                                 String arrayName) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        Map<String, ?> keys = settings.getAll();

        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            Logger.d("map values", entry.getKey() + ": "
                    + entry.getValue().toString());
            if (entry.getKey().contains(arrayName + "_"))
                editor.remove(entry.getKey());
        }
        editor.putInt(arrayName + "_size", array.length);
        for (int i = 0; i < array.length; i++)
            editor.putString(arrayName + "_" + i, array[i]);
    }

    public static String[] loadArray(Context context, String arrayName) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        int size = settings.getInt(arrayName + "_size", 0);
        String array[] = new String[size];
        for (int i = 0; i < size; i++)
            array[i] = settings.getString(arrayName + "_" + i, null);

        Logger.e("Loaded Array Size", array.length + "");
        return array;
    }

    public static int getArraySize(Context context, String arrayName) {
        SharedPreferences settings = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        return settings.getInt(arrayName + "_size", 0);
    }

    public static void clearArray(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(
                Valuepad_PREFRENCE, 0);
        SharedPreferences.Editor editor = prefs.edit();
        Map<String, ?> keys = prefs.getAll();

        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            Logger.d("map values", entry.getKey() + ": "
                    + entry.getValue().toString());
            if (entry.getKey().contains(Preferences.NotificationArrayName + "_"))
                editor.remove(entry.getKey());
        }
        editor.putInt(Preferences.NotificationArrayName + "_size", 0);
        editor.putString(Preferences.NotificationArrayName + "_", "");
        Logger.e("Save Succeed?", editor.commit() + "");
    }
}
