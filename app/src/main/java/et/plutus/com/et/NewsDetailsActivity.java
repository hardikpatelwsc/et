package et.plutus.com.et;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.HashMap;

import et.plutus.com.et.common.Logger;
import et.plutus.com.et.common.ratioTransformation;
import et.plutus.com.et.model.News;
import et.plutus.com.et.webServices.API;
import et.plutus.com.et.webServices.AsyncTaskListener;
import et.plutus.com.et.webServices.JSONHelper;

import static android.R.attr.id;

public class NewsDetailsActivity extends Activity implements View.OnClickListener {

    private Context mContext = NewsDetailsActivity.this;
    private ImageView imgNews;
    private TextView txtNewsTitle;
    private TextView txtNewsDesc;
    private TextView txtNewsHeading;
    private TextView txtNewsInfo;
    private TextView title_news_header;
    private String newsfeed_id = "";
    private ImageView img_back_news, img_share_news;
    private LinearLayout info_layout;
//    private TextView txt_docs_heading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_details);
        newsfeed_id = getIntent().getStringExtra("newsfeed_id");
        Log.e("News_ID_Detail", "" + newsfeed_id);
        initViews();
        getNewsDetails();
//        setInfo();

    }

// INit Views which are used
    private void initViews() {
        title_news_header = (TextView) findViewById(R.id.title_news_header);
        img_back_news = (ImageView) findViewById(R.id.img_back_news);
        img_share_news = (ImageView) findViewById(R.id.img_share_news);
        imgNews = (ImageView) findViewById(R.id.img_news);
        txtNewsTitle = (TextView) findViewById(R.id.txt_news_title);
        txtNewsDesc = (TextView) findViewById(R.id.txt_news_desc);
        txtNewsHeading = (TextView) findViewById(R.id.txt_news_heading);
        txtNewsInfo = (TextView) findViewById(R.id.txt_news_info);
        info_layout = (LinearLayout) findViewById(R.id.info_layout);
//        txt_docs_heading = (TextView) findViewById(R.id.txt_docs_heading);
        img_back_news.setOnClickListener(this);
        img_share_news.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back_news:
                onBackPressed();
                break;

            case R.id.img_share_news:
                shareApp();
                break;
        }
    }

// Sharing the news
    void shareApp() {
        String shareBody = "Sharing news via application";
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Event Management");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Event Management news share via"));
    }

    // Getting the news details by using newsfeed_id
    public void getNewsDetails() {
        //Make Post Params
        HashMap<String, String> params = new HashMap<>();
        params.put("newsfeed_id", newsfeed_id);
        String postParameters = JSONHelper.getJsonObjectString(params);
        ////////////////////////
        API api = new API(mContext, true, new AsyncTaskListener() {
            @Override
            public void onFailure(String FailureCause) {
                Log.e("Failure: ", FailureCause + "");
            }

            @Override
            @TargetApi(24)
            public void responseExtractLogic(String response) {
                try {
                    JSONObject obj_main = new JSONObject(response);
                    Logger.e("Response" + response);

                    JSONObject obj_data = obj_main.getJSONObject("data");
                        News news = new News();
                        JSONObject obj_news = obj_data.getJSONObject("Newsfeed");
                        news.setNews_id(obj_news.getString("id"));
                        news.setNews_title(obj_news.getString("title"));
                        news.setNews_description(obj_news.getString("description"));
                        news.setNews_publish_date(obj_news.getString("publish_date"));
                        news.setNews_image(obj_news.getString("image"));

                        Log.e("News_Id", "" + id);
//                        Log.e("News_Image", "" + obj_news.getString("image_full_path"));

                        title_news_header.setText(obj_news.getString("title"));
                    if(BuildConfig.VERSION_CODE<24)
                        txtNewsInfo.setText(Html.fromHtml(obj_news.getString("description")));
                    else
                        txtNewsInfo.setText(Html.fromHtml(obj_news.getString("description"),Html.FROM_HTML_MODE_LEGACY));
                        txtNewsHeading.setText(obj_news.getString("title"));
                        txtNewsTitle.setText(obj_news.getString("title"));
                        txtNewsDesc.setText(obj_news.getString("title"));

                        if (!obj_news.getString("image").equals("")) {
                            news.setNews_image_path(obj_news.getString("image_full_path"));
                            Picasso.with(mContext).load(obj_news.getString("image_full_path"))
                                    .transform(new ratioTransformation(imgNews))
                                    .into(imgNews);
                        }



                } catch (Exception E) {
                    onFailure(API.MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                }
            }
        });
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, API.API_GET_NEWS, postParameters);


    }
}
