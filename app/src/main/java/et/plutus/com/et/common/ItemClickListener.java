package et.plutus.com.et.common;

import android.view.View;

/**
 * Created by Ketan on 02-02-2016.
 */
public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
