package et.plutus.com.et.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import et.plutus.com.et.MainActivity;
import et.plutus.com.et.R;
import et.plutus.com.et.adapter.NewsAdapter;
import et.plutus.com.et.common.Logger;
import et.plutus.com.et.common.MyCustomLayoutManager;
import et.plutus.com.et.model.News;
import et.plutus.com.et.webServices.API;
import et.plutus.com.et.webServices.AsyncTaskListener;
import et.plutus.com.et.webServices.JSONHelper;

import static android.R.attr.id;

/**
 * Created by Ketan on 03-05-2016.
 */
public class NewsFragment extends Fragment implements SearchView.OnQueryTextListener, View.OnClickListener {

    private Context mContext;
    private RecyclerView recycler_view_news;
    private MyCustomLayoutManager mLayoutManager;
    private ArrayList<News> news_list_model = new ArrayList<News>();
    private NewsAdapter adapter_news;
    SearchView searchView_filter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle bundle) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        setHasOptionsMenu(true);
        mContext = getActivity();
        initViews(view);
        news_list_model.clear();
        getNewsList();
        return view;
    }

    // Setting News Adapter into Recycler view
    public void setAdapter() {
        adapter_news = new NewsAdapter(mContext, news_list_model);
        recycler_view_news.setAdapter(adapter_news);
    }

    // Initiating Used Views
    void initViews(View view) {
        setHasOptionsMenu(true);
        recycler_view_news = (RecyclerView) view.findViewById(R.id.recycler_view_news);
        mLayoutManager = new MyCustomLayoutManager(mContext);
        recycler_view_news.setLayoutManager(mLayoutManager);
        recycler_view_news.setHasFixedSize(true);
        recycler_view_news.addItemDecoration(new HorizontalDividerItemDecoration.Builder(mContext).build());
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDestroy() {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    // Getting News List
    public void getNewsList() {
        //Make Post Params
        HashMap<String, String> params = new HashMap<>();
//        params.put("newsfeed_id", "");
        String postParameters = JSONHelper.getJsonObjectString(params);
        ////////////////////////
        API api = new API(mContext, true, new AsyncTaskListener() {
            @Override
            public void onFailure(String FailureCause) {
                Log.e("Failure: ", FailureCause + "");
            }

            @Override
            public void responseExtractLogic(String response) {
                try {
                    JSONObject obj_main = new JSONObject(response);
                    Logger.e("Response" + response);
                    if (obj_main.optInt("status") == 1) {
                        JSONArray arr_data = obj_main.getJSONArray("data");
                        if (arr_data.length() > 0) {
                            for (int i = 0; i < arr_data.length(); i++) {
                                News news = new News();
                                JSONObject obj_data = arr_data.optJSONObject(i);
                                JSONObject obj_news = obj_data.getJSONObject("Newsfeed");
                                news.setNews_id(obj_news.getString("id"));
                                news.setNews_title(obj_news.getString("title"));
                                news.setNews_description(obj_news.getString("description"));
                                news.setNews_publish_date(obj_news.getString("publish_date"));
                                news.setNews_image(obj_news.getString("image"));
                                if (!obj_news.getString("image").equals(""))
                                    news.setNews_image_path(obj_news.getString("image_full_path"));
                                Log.e("News_Id", "" + id);
                                news_list_model.add(news);

                            }
                            setAdapter();
                        } else {
                            Toast.makeText(mContext, "News data not available", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mContext, obj_main.optString("message"), Toast.LENGTH_SHORT).show();
                    }

//                    boolean Success = obj_main.getBoolean("success");
//                    if (!Success) {
//                        APIUtilities.ShowAlertWithOneButton("", obj_main.getString("message"), API.OK, true, mContext);
//                    } else {
//                        Logger.e("Response" + Success);
//                    }
                } catch (Exception E) {
                    onFailure(API.MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                }
            }
        });
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, API.API_GET_NEWS, postParameters);


    }


    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        final ImageView mCloseButton = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        adapter_news.setFilter(news_list_model);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
        Log.e("isIconified", "" + searchView.isIconfiedByDefault());
        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView.setQuery("", false);
                //Collapse the action view
                searchView.onActionViewCollapsed();
                //Collapse the search widget
                item.collapseActionView();
            }
        });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<News> filteredModelList = filter(news_list_model, newText);
        adapter_news.setFilter(filteredModelList);

//        Log.e("isIconified", "" + searchView.isIconfiedByDefault());
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<News> filter(List<News> models, String query) {
        query = query.toLowerCase();

        final List<News> filteredModelList = new ArrayList<>();
        for (News model : models) {
            final String text = model.getNews_title().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


}
