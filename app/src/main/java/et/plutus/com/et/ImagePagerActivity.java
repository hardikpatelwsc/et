package et.plutus.com.et;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import et.plutus.com.et.adapter.FullScreenImagePagerAdapter;

public class ImagePagerActivity extends Activity implements View.OnClickListener {

    private Context mContext = ImagePagerActivity.this;
    private ViewPager vp_images;
    private ImageView img_back_news;
    int initialPosition=0;
//    private ArrayList<String> Images = new ArrayList<>();
    TextView title_news_header;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagepager);
        initialPosition = getIntent().getIntExtra("position",0);
        initInfo();
    }

    // Initialize and set Views
    private void initInfo() {
        title_news_header = (TextView) findViewById(R.id.title_news_header);
        title_news_header.setText(EventsDetailsActivity.title_news_header.getText().toString().trim());
        img_back_news = (ImageView) findViewById(R.id.img_back_news);
        vp_images = (ViewPager) findViewById(R.id.vp_images);

        img_back_news.setOnClickListener(this);
        // setting Zoomable Images Adapter into Gallery ViewPager
        FullScreenImagePagerAdapter pagerAdapter = new FullScreenImagePagerAdapter(mContext, EventsDetailsActivity.Images);
        vp_images.setAdapter(pagerAdapter);
        vp_images.setCurrentItem(initialPosition);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back_news:
                onBackPressed();
                break;
        }
    }


}
