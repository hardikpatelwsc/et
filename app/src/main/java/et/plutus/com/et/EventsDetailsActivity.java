package et.plutus.com.et;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roomorama.caldroid.Event;
import com.squareup.picasso.Picasso;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import et.plutus.com.et.adapter.DocumentAdapter;
import et.plutus.com.et.adapter.ImagePagerAdapter;
import et.plutus.com.et.common.Logger;
import et.plutus.com.et.common.MyCustomLayoutManager;
import et.plutus.com.et.common.ratioTransformation;
import et.plutus.com.et.model.Document;
import et.plutus.com.et.webServices.API;
import et.plutus.com.et.webServices.AsyncTaskListener;
import et.plutus.com.et.webServices.JSONHelper;

public class EventsDetailsActivity extends Activity implements View.OnClickListener {

    public static TextView title_news_header;
    public static ArrayList<String> Images = new ArrayList<>();
    LinearLayout ll_showOnYT;
    String YoutubeLink = "";
    String ShareLink = "";
    private Context mContext = EventsDetailsActivity.this;
    private ViewPager vp_events;
    private ImageView imgNews;
    private TextView txtNewsTitle;
    private TextView txtNewsDesc;
    private TextView txtNewsHeading;
    private TextView txtNewsInfo;
    private TextView txt_docs_not_available;
    private String event_id = "";
    private ImageView img_back_news, img_share_news;
    private TextView btn_info, btn_documents;
    private LinearLayout info_layout, doc_layout;
    private RecyclerView rv_doc;
    private DocumentAdapter adapter_doc;
    private ArrayList<Document> doc_list = new ArrayList<Document>();
    private MyCustomLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events_details);
        event_id = getIntent().getStringExtra("event_id");
        Images.clear();
        initViews();
        getEventDetails();
    }

    // Initialize and set the Views used in the Screen
    private void initViews() {
        title_news_header = (TextView) findViewById(R.id.title_news_header);
        img_back_news = (ImageView) findViewById(R.id.img_back_news);
        img_share_news = (ImageView) findViewById(R.id.img_share_news);
        vp_events = (ViewPager) findViewById(R.id.vp_events);
        imgNews = (ImageView) findViewById(R.id.img_news);
        txtNewsTitle = (TextView) findViewById(R.id.txt_news_title);
        txtNewsDesc = (TextView) findViewById(R.id.txt_news_desc);
        txtNewsHeading = (TextView) findViewById(R.id.txt_news_heading);
        txtNewsInfo = (TextView) findViewById(R.id.txt_news_info);
        btn_info = (TextView) findViewById(R.id.btn_info);
        btn_documents = (TextView) findViewById(R.id.btn_documents);
        info_layout = (LinearLayout) findViewById(R.id.info_layout);
        doc_layout = (LinearLayout) findViewById(R.id.doc_layout);
//        txt_docs_heading = (TextView) findViewById(R.id.txt_docs_heading);
        ll_showOnYT = (LinearLayout) findViewById(R.id.ll_showOnYT);
        txt_docs_not_available = (TextView) findViewById(R.id.txt_docs_not_available);
        rv_doc = (RecyclerView) findViewById(R.id.rv_doc);

        mLayoutManager = new MyCustomLayoutManager(mContext);
        rv_doc.setLayoutManager(mLayoutManager);
        rv_doc.setHasFixedSize(true);
        rv_doc.addItemDecoration(new HorizontalDividerItemDecoration.Builder(mContext).build());

        img_back_news.setOnClickListener(this);
        btn_info.setOnClickListener(this);
        btn_documents.setOnClickListener(this);
        img_share_news.setOnClickListener(this);
        ll_showOnYT.setOnClickListener(this);
        img_share_news.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back_news:
                onBackPressed();
                break;
            case R.id.btn_info:
                setInfoDetails();
                break;
            case R.id.btn_documents:
                setDocDetails();
                break;
            case R.id.img_share_news:
                shareApp();
                break;
            case R.id.ll_showOnYT:
                if (YoutubeLink.length() > 0) // if Youtube Link would be there in the Data, length of YoutubeLink would be greater than 0.
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(YoutubeLink)));// Opening YT app if link is available
                break;
        }
    }

    // for showing Info on Info Tab getting selected
    void setInfoDetails() {
        btn_info.setTypeface(null, Typeface.BOLD);
        btn_documents.setTypeface(null, Typeface.NORMAL);
        info_layout.setVisibility(View.VISIBLE);
        doc_layout.setVisibility(View.GONE);
        btn_info.setBackgroundColor(getResources().getColor(R.color.selected_txt_bg));
        btn_documents.setBackgroundColor(getResources().getColor(R.color.default_txt_bg));
    }

    // for showing Docs on Docs Tab getting selected
    void setDocDetails() {
        btn_info.setTypeface(null, Typeface.NORMAL);
        btn_documents.setTypeface(null, Typeface.BOLD);
        doc_layout.setVisibility(View.VISIBLE);
        info_layout.setVisibility(View.GONE);
        btn_info.setBackgroundColor(getResources().getColor(R.color.default_txt_bg));
        btn_documents.setBackgroundColor(getResources().getColor(R.color.selected_txt_bg));
    }

    //Share Event Link if sharing link is available, else share Event Details by concating the Event details, available photos and YT link if available
    void shareApp() {
        StringBuilder shareBody = new StringBuilder();
        if (ShareLink.length() > 0)
            shareBody.append(ShareLink);
        else {
            shareBody.append(txtNewsInfo.getText().toString());
            if (Images.size() > 0) {
                shareBody.append("Have a look @ few images");
                for (int i = 0; i < Images.size(); i++) {
                    shareBody.append("\n\n" + Images.get(i));
                }
                if (YoutubeLink.length() > 0)
                    shareBody.append("\n\nAlso watch on Youtube: " + YoutubeLink);
            } else {
                if (YoutubeLink.length() > 0)
                    shareBody.append("\n\nWatch on Youtube: " + YoutubeLink);
            }

        }
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Event Management");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody.toString());
        startActivity(Intent.createChooser(sharingIntent, "Event Management news share via"));
    }

    // CAlling Event Details API and getting event data
    public void getEventDetails() {
        //Make Post Params
        HashMap<String, String> params = new HashMap<>();
        params.put("event_id", event_id);
        String postParameters = JSONHelper.getJsonObjectString(params);
        ////////////////////////
        API api = new API(mContext, true, new AsyncTaskListener() {
            @Override
            public void onFailure(String FailureCause) {
                Log.e("Failure: ", FailureCause + "");
            }

            @Override
            @TargetApi(24)
            public void responseExtractLogic(String response) {
                try {
                    JSONObject obj_main = new JSONObject(response);
                    Logger.e("Response" + response);

                    JSONObject obj_data = obj_main.getJSONObject("data");
                    Event event = new Event();
                    JSONObject obj_news = obj_data.getJSONObject("Event");
                    event.setEvent_id(obj_news.getString("id"));
                    event.setEvent_title(obj_news.getString("event_name"));
                    event.setEvent_description(obj_news.getString("description"));
                    event.setEvent_publish_date(obj_news.getString("created_date"));
                    event.setEvent_image_name(obj_news.getString("event_image_path"));
                    ShareLink = obj_news.has("share_url") ? (obj_news.getString("share_url")) : "";
                    String ImageURL = obj_news.has("event_image_full_path") ? (obj_news.getString("event_image_full_path")) : "";
                    if (!TextUtils.isEmpty(ImageURL))
                        Picasso.with(mContext).load(ImageURL).transform(new ratioTransformation(imgNews)).placeholder(R.drawable.event_placeholder).into(imgNews);

                    title_news_header.setText(obj_news.getString("event_name"));

                    if(BuildConfig.VERSION_CODE<24)
                        txtNewsInfo.setText(Html.fromHtml(obj_news.getString("description")));
                    else
                        txtNewsInfo.setText(Html.fromHtml(obj_news.getString("description"),Html.FROM_HTML_MODE_LEGACY));

                    txtNewsHeading.setText(obj_news.getString("event_name"));
                    txtNewsTitle.setText(obj_news.getString("event_name"));
                    txtNewsDesc.setText(obj_news.getString("event_name"));

                    JSONArray arr_Doc = obj_data.optJSONArray("Document");
                    for (int i = 0; i < arr_Doc.length(); i++) {
                        JSONObject obj_Doc = arr_Doc.optJSONObject(i);
                        String type = obj_Doc.optString("type");
                        if (type.equalsIgnoreCase("Image")) {
                            JSONArray Doc_Images = obj_Doc.optJSONArray("document_full_path");
                            if (obj_Doc.optString("document_path").length() > 0)
                                for (int j = 0; j < Doc_Images.length(); j++) {
                                    Images.add(Doc_Images.getString(j));
                                    Log.e("Image Path:", Doc_Images.getString(j) + "");
                                }
                            event.setEvent_images(Images);
                        } else {
                            if (type.equalsIgnoreCase("pdf")) {
                                Document doc = new Document();
                                JSONArray Doc_PDF = obj_Doc.optJSONArray("document_full_path");
                                if (obj_Doc.optString("document_path").length() > 0)
                                    for (int j = 0; j < Doc_PDF.length(); j++) {
                                        doc.setDoc_type("PDF");
                                        String url = Doc_PDF.getString(j);
                                        doc.setDoc_title(url.substring(url.lastIndexOf("/") + 1));
                                        doc.setDoc_url(url);
                                        doc_list.add(doc);
                                        Log.e("PDF Path:", Doc_PDF.getString(j) + "");
                                    }
                            } else if (type.equalsIgnoreCase("File")) {
                                Document doc = new Document();
                                JSONArray Doc_File = obj_Doc.optJSONArray("document_full_path");
                                if (obj_Doc.optString("document_path").length() > 0)
                                    for (int j = 0; j < Doc_File.length(); j++) {
                                        doc.setDoc_type("TXT");
                                        String url = Doc_File.getString(j);
                                        doc.setDoc_title(url.substring(url.lastIndexOf("/") + 1));
                                        doc.setDoc_url(url);
                                        doc_list.add(doc);
                                        Log.e("TXT Path:", Doc_File.getString(j) + "");
                                    }
                            } else if (type.equalsIgnoreCase("URL")) {
                                if (obj_Doc.optString("document_path").length() > 0) {
                                    // Setting YoutubeLink and showing YT button which is invisible by default
                                    YoutubeLink = obj_Doc.optString("document_path");
                                    ll_showOnYT.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }
                    // Setting Image adapter for showing into Gallery ViewPager
                    if (!obj_news.getString("event_image_path").equals("") && Images.size() > 0) {
                        Log.e("TotAl Images", Images.size() + "");
                        ImagePagerAdapter pagerAdapter = new ImagePagerAdapter(EventsDetailsActivity.this, Images);
                        vp_events.setAdapter(pagerAdapter);
                        imgNews.setVisibility(View.GONE);
//                        Picasso.with(mContext).load(obj_news.getString("event_image_full_path"))
//                                .placeholder(R.drawable.logo)
//                                .into(imgNews);
                    }
                    if (doc_list.size() > 0) {
                        adapter_doc = new DocumentAdapter(mContext, doc_list);
                        rv_doc.setAdapter(adapter_doc);
                    } else {
                        rv_doc.setVisibility(View.GONE);
                        txt_docs_not_available.setVisibility(View.VISIBLE);
                    }


                } catch (Exception E) {
                    onFailure(API.MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                }
            }
        });
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, API.API_GET_EVENTS, postParameters);
    }
}
