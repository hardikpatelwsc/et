package et.plutus.com.et;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONObject;

import java.util.HashMap;

import et.plutus.com.et.common.Logger;
import et.plutus.com.et.common.Preferences;
import et.plutus.com.et.webServices.API;
import et.plutus.com.et.webServices.AsyncTaskListener;
import et.plutus.com.et.webServices.JSONHelper;

public class SplashActivity extends AppCompatActivity {
    private Context mContext = SplashActivity.this;
    private int SPLASH_DISPLAY_LENGTH = 3000;
    private Thread Syncing = null;
    private String Came_From = "";
    private String notification_id = "";
    private String notification_type = "";
    public static boolean flag_push = false;
    private String token;
    TextView tv_version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        initView();
        getFCMToken();

        if (Preferences.isIntro.equals(""))
            Preferences.setValue(mContext, Preferences.isIntro, false);

        checkNotificationClicked();

        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
    }

    // we need to check if the app has been opened by clicking on the notification or the app icon itself.
    private void checkNotificationClicked() {
        try {
            Came_From = getIntent().getStringExtra("Came_From");
            if (Came_From.equals("FCM_Service")) {
                notification_id = getIntent().getStringExtra("notification_id");
                notification_type = getIntent().getStringExtra("notification_type");

                Logger.e("SPLASH notification_id : "+notification_id);
                Preferences.setValue(mContext, Preferences.FCM_TYPE, notification_type);
                Preferences.setValue(mContext, Preferences.FCM_ID, notification_id);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!TextUtils.isEmpty(Came_From)) {
        /*Handling Response of FCM and Navigate to their required activity */
            new Handler().post(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    try {
                        flag_push = true;
                        Intent i = new Intent(mContext, MainActivity.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Intent mainAct = new Intent(mContext, MainActivity.class);
                    startActivity(mainAct);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    finish();

                }
            }, SPLASH_DISPLAY_LENGTH);
        }
    }

    // getting FCMToken and saving it to shared preferences .
    private void getFCMToken() {
        Syncing = new Thread() {
            public void run() {
                try {
                    InstanceID instanceID = InstanceID.getInstance(mContext);
                    try {
                        token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                        Logger.e("FCMToken", "InstanceID token: " + token);
                        Preferences.setValue(mContext,
                                Preferences.FCM_Token, token);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Syncing.start();
    }

    // Initialize views used
    private void initView() {
        tv_version = (TextView) findViewById(R.id.tv_version);
        tv_version.setText(BuildConfig.VERSION_NAME);
    }


}
