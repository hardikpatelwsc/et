package et.plutus.com.et.caldroid;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidGridAdapter;

import java.util.Locale;
import java.util.Map;

import et.plutus.com.et.R;
import hirondelle.date4j.DateTime;

public class CaldroidSampleCustomAdapter extends CaldroidGridAdapter {

    final String LongEventformat = "DD MMM YYYY";
    private Context mContext;

    public CaldroidSampleCustomAdapter(Context context, int month, int year,
                                       Map<String, Object> caldroidData,
                                       Map<String, Object> extraData) {
        super(context, month, year, caldroidData, extraData);
        mContext = context;
    }

    public static void setViewColor(View view, int Color) {
        GradientDrawable gd = (GradientDrawable) view.getBackground().getCurrent();
        gd.setColor(Color/*Color.parseColor("#000000")*/);
//        gd.setCornerRadii(new float[]{30, 30, 30, 30, 0, 0, 30, 30});
//        gd.setStroke(2, android.graphics.Color.parseColor("#FFFFFF"), 5, 6);

        //use this for layerDrawable
//        LayerDrawable bgDrawable = (LayerDrawable)view.getBackground();
//        final GradientDrawable shape = (GradientDrawable)   bgDrawable.findDrawableByLayerId(R.id.shape_id);
//        shape.setColor(xxxxxxxx);
    }

    public static int getViewColor(View view) {
//        Drawable viewBackground = view.getBackground();
//        if (viewBackground instanceof ColorDrawable)
        return ((ColorDrawable) view.getBackground()).getColor();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cellView = convertView;

        // For reuse
        if (convertView == null) {
            cellView = inflater.inflate(R.layout.custom_cell, null);
        }

        int topPadding = cellView.getPaddingTop();
        int leftPadding = cellView.getPaddingLeft();
        int bottomPadding = cellView.getPaddingBottom();
        int rightPadding = cellView.getPaddingRight();

        TextView tv_date = (TextView) cellView.findViewById(R.id.tv_date);
        TextView tv_event = (TextView) cellView.findViewById(R.id.tv_event);
        TextView tv_event2 = (TextView) cellView.findViewById(R.id.tv_event2);
        View start_line = cellView.findViewById(R.id.start_line);
        View continue_line = cellView.findViewById(R.id.continue_line);
        View end_line = cellView.findViewById(R.id.end_line);
        View cs_line = cellView.findViewById(R.id.cs_line);
        View ec_line = cellView.findViewById(R.id.ec_line);

        start_line.setVisibility(View.GONE);
        continue_line.setVisibility(View.GONE);
        end_line.setVisibility(View.GONE);
        cs_line.setVisibility(View.GONE);
        ec_line.setVisibility(View.GONE);

        tv_date.setTextColor(Color.BLACK);

        // Get dateTime of this cell
        DateTime dateTime = this.datetimeList.get(position);
        Resources resources = context.getResources();
        String Date = dateTime.format(LongEventformat, Locale.ENGLISH);


        boolean shouldResetDiabledView = false;
        boolean shouldResetSelectedView = false;

        // Customize for disabled dates and date outside min/max dates
        if ((minDateTime != null && dateTime.lt(minDateTime))
                || (maxDateTime != null && dateTime.gt(maxDateTime))
                || (disableDates != null && disableDates.indexOf(dateTime) != -1)) {

            tv_date.setTextColor(CaldroidFragment.disabledTextColor);
            if (CaldroidFragment.disabledBackgroundDrawable == -1) {
                cellView.setBackgroundResource(com.caldroid.R.drawable.disable_cell);
            } else {
                cellView.setBackgroundResource(CaldroidFragment.disabledBackgroundDrawable);
            }

            if (dateTime.equals(getToday())) {
                cellView.setBackgroundResource(com.caldroid.R.drawable.red_border_gray_bg);
            }

        } else {
            shouldResetDiabledView = true;
        }

        // Customize for selected dates
        if (selectedDates != null && selectedDates.indexOf(dateTime) != -1) {
            cellView.setBackgroundColor(resources
                    .getColor(com.caldroid.R.color.caldroid_sky_blue));

            tv_date.setTextColor(Color.BLACK);

        } else {
            shouldResetSelectedView = true;
        }

        if (shouldResetDiabledView && shouldResetSelectedView) {
            // Customize for today
            if (dateTime.equals(getToday())) {
                cellView.setBackgroundResource(com.caldroid.R.drawable.bg_cell_border_today);
                tv_date.setTextColor(mContext.getResources().getColor(R.color.badge_red));
                tv_date.setTypeface(null, Typeface.BOLD);
            } else {
                tv_date.setTextColor(mContext.getResources().getColor(R.color.caldroid_middle_gray));
                cellView.setBackgroundResource(com.caldroid.R.drawable.bg_cell_border);
            }
        }


        // Customize for disabled dates and date outside min/max dates
        if ((eventDates != null && eventDates.indexOf(dateTime) != -1) && dateTime.getMonth() == month) {
            Log.e("EVENTS", eventDates.indexOf(dateTime) + "###");
            String[] EVEs = events.get(eventDates.indexOf(dateTime)).split("#");
            String Event = EVEs[0];
            if (Event.length() > 0 && !Event.equals("0")) {
                tv_event.setText(Event);
                tv_event.setVisibility(View.VISIBLE);
                tv_event2.setVisibility(View.VISIBLE);
            } else {
                tv_event.setVisibility(View.INVISIBLE);
                tv_event2.setVisibility(View.INVISIBLE);
            }
            if (longEvents.contains(Date)) {
                int index = longEvents.indexOf(Date);
                setViewColor(tv_event2, longEventsColor.get(index)[1]);
                if (longEventsType.get(index).equals(CaldroidFragment.START)) {
                    start_line.setVisibility(View.VISIBLE);
                    start_line.setBackgroundColor(longEventsColor.get(index)[2]);
                } else if (longEventsType.get(index).equals(CaldroidFragment.CONTINUOUS)) {
                    continue_line.setVisibility(View.VISIBLE);
                    continue_line.setBackgroundColor(longEventsColor.get(index)[1]);
                } else if (longEventsType.get(index).equals(CaldroidFragment.END)) {
                    end_line.setVisibility(View.VISIBLE);
                    end_line.setBackgroundColor(longEventsColor.get(index)[0]);
                } else if (longEventsType.get(index).equals(CaldroidFragment.END_START)) {
                    ec_line.setVisibility(View.VISIBLE);
                    cs_line.setVisibility(View.VISIBLE);
                    ec_line.setBackgroundColor(longEventsColor.get(index)[0]);
                    cs_line.setBackgroundColor(longEventsColor.get(index)[2]);
                } else if (longEventsType.get(index).equals(CaldroidFragment.CONTINUOUS_START)) {
                    ec_line.setVisibility(View.VISIBLE);
                    start_line.setVisibility(View.VISIBLE);
                    ec_line.setBackgroundColor(longEventsColor.get(index)[0]);
                    start_line.setBackgroundColor(longEventsColor.get(index)[2]);
                } else if (longEventsType.get(index).equals(CaldroidFragment.END_CONTINUOUS)) {
                    cs_line.setVisibility(View.VISIBLE);
                    end_line.setVisibility(View.VISIBLE);
                    end_line.setBackgroundColor(longEventsColor.get(index)[0]);
                    cs_line.setBackgroundColor(longEventsColor.get(index)[2]);
                }
            } else {
                setViewColor(tv_event2, mContext.getResources().getColor(R.color.badge_red));
            }
        } else {
            tv_event.setVisibility(View.INVISIBLE);
            tv_event2.setVisibility(View.INVISIBLE);
        }


        // Set color of the dates in previous / next month
        if (dateTime.getMonth() != month) {
            tv_date.setTextColor(resources
                    .getColor(com.caldroid.R.color.caldroid_white));
        }

        tv_date.setText("" + dateTime.getDay());

        // Somehow after setBackgroundResource, the padding collapse.
        // This is to recover the padding
        cellView.setPadding(leftPadding, topPadding, rightPadding,
                bottomPadding);

        // Set custom color if required
        setCustomResources(dateTime, cellView, tv_date);

        return cellView;
    }

}
