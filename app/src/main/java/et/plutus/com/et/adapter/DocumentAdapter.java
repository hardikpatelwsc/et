package et.plutus.com.et.adapter;

/**
 * Created by Ketan on 25-01-2016.
 */

import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import et.plutus.com.et.R;
import et.plutus.com.et.common.Logger;
import et.plutus.com.et.model.Document;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.CustomViewHolder> {
    //    private List<FeedItem> feedItemList;
    private Context mContext;
    private ArrayList<Document> docList;
    private ArrayList<Document> orig;
    private int count = 0;
    private String str_image = "";

    public DocumentAdapter(Context context, ArrayList<Document> docList) {
//        this.feedItemList = feedItemList;
        this.mContext = context;
        this.docList = docList;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_document, null);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, final int position) {

        customViewHolder.tv_doc_title.setText(docList.get(position).getDoc_title());
//        customViewHolder.tv_doc_url.setText(docList.get(position).getDoc_url());
        customViewHolder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Toast.makeText(mContext, "POsition" + position, Toast.LENGTH_SHORT).show();

                displayAlert("Do you want to download " + docList.get(position).getDoc_title()+"?", docList.get(position).getDoc_title(), docList.get(position).getDoc_url());

            }
        });

        if (docList.get(position).getDoc_type().equalsIgnoreCase("PDF"))
            Picasso.with(mContext).load(R.drawable.ic_pdf)
                    .into(customViewHolder.doc_type);
        else if (docList.get(position).getDoc_type().equalsIgnoreCase("TXT"))
            Picasso.with(mContext).load(R.drawable.ic_txt)
                    .into(customViewHolder.doc_type);
        else
            Picasso.with(mContext).load(R.drawable.ic_unknown_doc)
                    .into(customViewHolder.doc_type);

//        if (!newsList.get(position).getNews_image().equals("")) {
//            str_image = newsList.get(position).getNews_image_path();
////            Glide.with(mContext).load(newsList.get(position).getNews_image_path())
////                    .into(customViewHolder.img_news);
////            Glide.with(mContext).load(str_image)
////                    .fitCenter()
////                    .crossFade()
////                    .into(customViewHolder.img_news);
//
//            Picasso.with(mContext).load(str_image)
//                    .placeholder(R.drawable.logo)
//                    .into(customViewHolder.doc_type);
//        }

    }

    @Override
    public int getItemCount() {
        return (null != docList ? docList.size() : 0);
    }

    public void downloadDocs(String docName, String docUrl) {
        try {
//            mContext.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
            DownloadManager.Request r = new DownloadManager.Request(Uri
                    .parse(docUrl));
            // This put the download in the same Download dir the
            // browser
            // uses
            r.setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_DOWNLOADS,
                    docName);

            // When downloading music and videos they will be listed in
            // the
            // player
            // (Seems to be available since Honeycomb only)
            r.allowScanningByMediaScanner();

            // Notify user when download is completed
            // (Seems to be available since Honeycomb only)
            r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            // Start download
            DownloadManager mgr = (DownloadManager) mContext
                    .getSystemService(mContext.DOWNLOAD_SERVICE);
            mgr.enqueue(r);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayAlert(String message, final String docName, final String docUrl) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(mContext);
        builder.setTitle("Event Documents");
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Logger.e("Document URl", "" + docUrl);
                downloadDocs(docName, docUrl.replace(" ", "%20"));
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView doc_type;
        protected TextView tv_doc_title, tv_doc_url;
        protected View view;

        public CustomViewHolder(View view) {
            super(view);
            this.view = view;
            this.doc_type = (ImageView) view.findViewById(R.id.doc_type);
            this.tv_doc_title = (TextView) view.findViewById(R.id.tv_doc_title);
            this.tv_doc_url = (TextView) view.findViewById(R.id.tv_doc_url);
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                }
//            });


        }

        private View getView() {
            return view;
        }
    }
}
