package et.plutus.com.et.model;

/**
 * Created by Ketan on 21-09-2016.
 */

public class Notification {

    String notification_id;
    String notification_title;
    String notification_description;
    String notification_show_id;

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }

    public String getNotification_title() {
        return notification_title;
    }

    public void setNotification_title(String notification_title) {
        this.notification_title = notification_title;
    }

    public String getNotification_description() {
        return notification_description;
    }

    public void setNotification_description(String notification_description) {
        this.notification_description = notification_description;
    }

    public String getNotification_show_id() {
        return notification_show_id;
    }

    public void setNotification_show_id(String notification_show_id) {
        this.notification_show_id = notification_show_id;
    }

    public String getNotification_publish_date() {
        return notification_publish_date;
    }

    public void setNotification_publish_date(String notification_publish_date) {
        this.notification_publish_date = notification_publish_date;
    }

    public String getNotification_publish_time() {
        return notification_publish_time;
    }

    public void setNotification_publish_time(String notification_publish_time) {
        this.notification_publish_time = notification_publish_time;
    }

    public boolean is_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    String notification_publish_date;
    String notification_publish_time;
    boolean is_deleted;
    String created_date;
    String updated_date;



}
