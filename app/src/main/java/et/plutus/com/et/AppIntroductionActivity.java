package et.plutus.com.et;

import android.os.Build;
import android.os.Bundle;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

import et.plutus.com.et.common.Preferences;

public class AppIntroductionActivity extends IntroActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Preferences.setValue(AppIntroductionActivity.this, Preferences.isIntro, true);

        // We can modify App Introduction UI by changing commented properties
//        setButtonBackVisible(true);
//        setButtonNextVisible(true);
        setButtonCtaVisible(false);
//        setButtonCtaTintMode(BUTTON_CTA_TINT_MODE_BACKGROUND);
//        TypefaceSpan labelSpan = new TypefaceSpan(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? "sans-serif-medium" : "sans serif");
//        SpannableString label = SpannableString.valueOf("Get started");
//        label.setSpan(labelSpan, 0, label.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        setButtonCtaLabel(label);

        // Setting automatic page scroll speed
        setPageScrollDuration(1000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setPageScrollInterpolator(android.R.interpolator.fast_out_slow_in);
        }
        // Adding Slides to the introduction
// Slide 1
        addSlide(new SimpleSlide.Builder()
                .title("Centralised Console with Distributed Access")
                .description("Information created Centrally for Distributed Access")
                .image(R.drawable.ic_distribution)
                .background(R.color.intro_color_1)
                .backgroundDark(R.color.intro_color_dark_1)
                .layout(R.layout.app_intro_slide)
                .build());

// Slide 2
        addSlide(new SimpleSlide.Builder()
                .title("Day-wise Event Schedule")
                .description("Allows user to access Event-wise Agenda, Details & Schedules")
                .image(R.drawable.ic_calendar)
                .background(R.color.intro_color_2)
                .backgroundDark(R.color.intro_color_dark_2)
                .layout(R.layout.app_intro_slide)
                .build());

// Slide 3
        addSlide(new SimpleSlide.Builder()
                .title("News & Notification")
                .description("Receive advance information & instant alerts")
                .image(R.drawable.ic_newspaper)
                .background(R.color.intro_color_3)
                .backgroundDark(R.color.intro_color_dark_3)
                .layout(R.layout.app_intro_slide)
                .build());

// Slide 4
        addSlide(new SimpleSlide.Builder()
                .title("Share your joy")
                .description("Access & Share content on various Social media platforms")
                .image(R.drawable.ic_share)
                .background(R.color.intro_color_4)
                .backgroundDark(R.color.intro_color_dark_4)
                .layout(R.layout.app_intro_slide)
                .build());
// Slide 5
        addSlide(new SimpleSlide.Builder()
                .title("SAAS Model")
                .description("Robust & Secure Server with Contemporary SAAS Implementation & Pricing Model")
                .image(R.drawable.ic_billing)
                .background(R.color.intro_color_5)
                .backgroundDark(R.color.intro_color_dark_5)
                .layout(R.layout.app_intro_slide)
                .build());

        // Enabling Automatic Page change every fixed second (here 7 seconds) for infinite number of times..
        autoplay(7000, INFINITE);

    }

    @Override
    public void onBackPressed() {
    }
}
