package et.plutus.com.et.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import et.plutus.com.et.R;
import et.plutus.com.et.adapter.NewsAdapter;
import et.plutus.com.et.common.MyCustomLayoutManager;
import et.plutus.com.et.model.Product;

/**
 * Created by Ketan on 03-05-2016.
 */
public class ExhibitorsFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    private RecyclerView recycler_view_news;
    private MyCustomLayoutManager mLayoutManager;
    private ArrayList<Product> news_list_model = new ArrayList<>();
    private NewsAdapter adapter_news;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle bundle) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        mContext = getActivity();

        initViews(view);
        for (int i = 0; i < 10; i++) {
            Product item = new Product();
            item.setProduct_name("jay" + i);
            news_list_model.add(item);

        }

//        setAdapter();
        return view;
    }

//    public void setAdapter() {
//
//        adapter_news = new NewsAdapter(mContext, news_list_model);
//        recycler_view_news.setAdapter(adapter_news);
//
//    }

    void initViews(View view) {

        recycler_view_news = (RecyclerView) view.findViewById(R.id.recycler_view_news);
        mLayoutManager = new MyCustomLayoutManager(mContext);
        recycler_view_news.setLayoutManager(mLayoutManager);
        recycler_view_news.setHasFixedSize(true);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDestroy() {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
