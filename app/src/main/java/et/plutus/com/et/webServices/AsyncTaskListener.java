package et.plutus.com.et.webServices;

/**
 * Created by HARDIKP on 08/19/2016.
 */

public interface AsyncTaskListener {
	void onFailure(String failureCause);
	void responseExtractLogic(String response);
}
