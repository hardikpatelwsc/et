package et.plutus.com.et;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.roomorama.caldroid.Event;
import com.squareup.picasso.Picasso;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import et.plutus.com.et.adapter.EventsAdapter;
import et.plutus.com.et.common.AppConstants;
import et.plutus.com.et.common.Logger;
import et.plutus.com.et.common.MyCustomLayoutManager;
import et.plutus.com.et.fragment.EventsFragment;
import et.plutus.com.et.webServices.API;
import et.plutus.com.et.webServices.AsyncTaskListener;
import et.plutus.com.et.webServices.JSONHelper;

public class EventListingDateWiseActivity extends AppCompatActivity implements View.OnClickListener {

    public static MyCustomLayoutManager mLayoutManager;
    public static ArrayList<Event> event_list_filtered = new ArrayList<>();// to collect the events on particular date
    public static TextView title_news_header;
    private Context mContext = EventListingDateWiseActivity.this;
    private ImageView img_back_news;
    private Date EventDate = null;
    private String EventDateExtra = "";
    private RecyclerView recycler_view_events;
    private EventsAdapter adapter_events;
    private ImageView img_add;
    private String ad_id;


    CountDownTimer getTimelyAdd = new CountDownTimer(20000, 20000) {
        // this is the countdowntimer for changing the ad every few seconds..
        @Override
        public void onTick(long l) {

        }

        @Override
        public void onFinish() {
            getAds();
            getTimelyAdd.start();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filtered_event_list);
        try {
            // getting event date from intent where it came from
            EventDateExtra = getIntent().getStringExtra("EventDate");
            EventDate = AppConstants.formatter_api.parse(EventDateExtra);
        } catch (Exception E) {
            EventDate = null;
        }
        if (EventDate == null)
            finish();
        Logger.e("EventDateExtra: ", EventDateExtra);
        initViews();
        setAdapter();
        // calling ad API for the first time and then starting countdowntimer to change the ad after few seconds
        getAds();
        getTimelyAdd.start();
    }

    // Filter the event list of Events fragment and then adding events to the filtered list
    public void setAdapter() {
        event_list_filtered.clear();
        for (int i = 0; i < EventsFragment.calHandler.event_list_model.size(); i++) {
            Event event = EventsFragment.calHandler.event_list_model.get(i);
            String StartDate = event.getEvent_start_date();
            String EndDate = event.getEvent_end_date();
            Date SDate = null, EDate = null;
            try {
                SDate = AppConstants.formatter_api.parse(StartDate);
                EDate = AppConstants.formatter_api.parse(EndDate);
            } catch (Exception E) {

            }
            if (AppConstants.dateBetween(EventDate, SDate, EDate) || EventDateExtra.equals(StartDate) || EventDateExtra.equals(EndDate)) {
                event_list_filtered.add(event);
            }
        }
        adapter_events = new EventsAdapter(mContext, event_list_filtered, false);
        recycler_view_events.setAdapter(adapter_events);
    }

    //Initialized and set the Views used in the screen
    void initViews() {
        title_news_header = (TextView) findViewById(R.id.title_news_header);
        img_back_news = (ImageView) findViewById(R.id.img_back_news);
        String TempDate = AppConstants.formatter.format(EventDate);
        title_news_header.setText(TempDate.startsWith("0") ? TempDate.substring(1) : TempDate);

        img_add = (ImageView) findViewById(R.id.img_ad);
        recycler_view_events = (RecyclerView) findViewById(R.id.recycler_view_events);
        mLayoutManager = new MyCustomLayoutManager(mContext);
        recycler_view_events.setLayoutManager(mLayoutManager);
        recycler_view_events.setHasFixedSize(true);
        recycler_view_events.addItemDecoration(new HorizontalDividerItemDecoration.Builder(mContext).build());

        img_back_news.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back_news:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onDestroy() {
        try {
            getTimelyAdd.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }

    //calling ad api and getting ad data
    public void getAds() {
        //Make Post Params
        HashMap<String, String> params = new HashMap<>();
//        params.put("event_id", event_id);
        String postParameters = JSONHelper.getJsonObjectString(params);
        ////////////////////////
        API api = new API(mContext, true, new AsyncTaskListener() {
            @Override
            public void onFailure(String FailureCause) {
                Log.e("Failure: ", FailureCause + "");
            }

            @Override
            public void responseExtractLogic(String response) {
                try {
                    JSONObject obj_main = new JSONObject(response);
                    Logger.e("Response" + response);

                    JSONObject obj_data = obj_main.getJSONObject("data");
                    JSONObject obj_Ad = obj_data.optJSONObject("Ad");
                    ad_id = obj_Ad.optString("id");
                    String img_Ad = obj_Ad.optString("ad_image_thumb_path");
                    Log.e("Image_AD", "" + img_Ad);

                    Picasso.with(mContext).load(img_Ad).fit()
                            .into(img_add);
                } catch (Exception E) {
                    onFailure(API.MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                }
            }
        }, false);
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, API.API_GET_ADS, postParameters);
    }


    public void viewAds(String ad_id) {
        //Make Post Params
        HashMap<String, String> params = new HashMap<>();
        params.put("ads_id", ad_id);
        String postParameters = JSONHelper.getJsonObjectString(params);
        ////////////////////////
        API api = new API(mContext, true, new AsyncTaskListener() {
            @Override
            public void onFailure(String FailureCause) {
                Log.e("Failure: ", FailureCause + "");
            }

            @Override
            public void responseExtractLogic(String response) {
                try {
                    Logger.e("Response" + response);
                } catch (Exception E) {
                    onFailure(API.MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                }
            }
        });
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, API.API_VIEW_ADS, postParameters);


    }

}
