package et.plutus.com.et;

import android.os.AsyncTask;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import et.plutus.com.et.common.Logger;
import et.plutus.com.et.common.Preferences;

/**
 * Created by Ketan on 21-06-2016.
 */
public class InstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.

        // TODO: Implement this method to send any registration to your app's servers.
//        sendRegistrationToServer(refreshedToken);
        new Thread(new Runnable() {
            public void run() {
                String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                Logger.d(TAG, "Refreshed token: " + refreshedToken);
                Preferences.setValue(getApplicationContext(), Preferences.FCM_Token, refreshedToken);
                new RegisterDeviceToken().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }).start();
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p/>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */

    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
    }

    class RegisterDeviceToken extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
//                Global globalData = new Global();
//                ParsedResponse pr = globalData.apiRegisterDevice(getApplicationContext(), Preferences.getValue_Integer(getApplicationContext(), Preferences.Appraiser_ID), Preferences.getValue_String(getApplicationContext(), Preferences.FCM_Token), Preferences.Platform);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

        }
    }
}
