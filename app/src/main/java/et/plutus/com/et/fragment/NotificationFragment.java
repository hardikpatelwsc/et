package et.plutus.com.et.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import et.plutus.com.et.R;
import et.plutus.com.et.adapter.NotificationAdapter;
import et.plutus.com.et.common.Logger;
import et.plutus.com.et.common.MyCustomLayoutManager;
import et.plutus.com.et.model.Notification;
import et.plutus.com.et.webServices.API;
import et.plutus.com.et.webServices.AsyncTaskListener;
import et.plutus.com.et.webServices.JSONHelper;

/**
 * Created by Ketan on 03-05-2016.
 */
public class NotificationFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    private RecyclerView recycler_view_notification;
    private MyCustomLayoutManager mLayoutManager;
    private ArrayList<Notification> notification_list_model = new ArrayList<>();
    private NotificationAdapter adapter_notification;
    private String str_news;
    private String str_text;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle bundle) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        mContext = getActivity();

        initViews(view);
        getNotificationsList();
        return view;
    }

    // Setting Notification Adapter into Recycler view
    public void setAdapter() {
        adapter_notification = new NotificationAdapter(mContext, notification_list_model);
        recycler_view_notification.setAdapter(adapter_notification);
    }

    void initViews(View view) {

        recycler_view_notification = (RecyclerView) view.findViewById(R.id.recycler_view_notifications);
        mLayoutManager = new MyCustomLayoutManager(mContext);
        recycler_view_notification.setLayoutManager(mLayoutManager);
        recycler_view_notification.setHasFixedSize(true);
        recycler_view_notification.addItemDecoration(new HorizontalDividerItemDecoration.Builder(mContext).build());

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDestroy() {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    // Getting Latest Notifications through API
    public void getNotificationsList() {
        //Make Post Params
        HashMap<String, String> params = new HashMap<>();
//        params.put("newsfeed_id", "");
        String postParameters = JSONHelper.getJsonObjectString(params);
        ////////////////////////
        API api = new API(mContext, true, new AsyncTaskListener() {
            @Override
            public void onFailure(String FailureCause) {
                Log.e("Failure: ", FailureCause + "");
            }

            @Override
            public void responseExtractLogic(String response) {
                try {
                    JSONObject obj_main = new JSONObject(response);
                    Logger.e("Response" + response);
                    if (obj_main.optInt("status") == 1) {
                        JSONArray arr_data = obj_main.getJSONArray("data");
                        if (arr_data.length() > 0) {
                            for (int i = 0; i < arr_data.length(); i++) {
                                JSONObject obj_data = arr_data.optJSONObject(i);
                                JSONObject obj_notification = obj_data.optJSONObject("Notification");
                                Notification notification = new Notification();
                                notification.setNotification_id(obj_notification.optString("id"));
                                notification.setNotification_title(obj_notification.optString("title"));
                                notification.setNotification_description(obj_notification.optString("description"));
                                notification_list_model.add(notification);
                            }
                            setAdapter();
                        } else {
                            Toast.makeText(mContext, "Notification data not available", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mContext, obj_main.optString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception E) {
                    onFailure(API.MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                }
            }
        });
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, API.API_GET_NOTIFICATIONS, postParameters);


    }

}
