package et.plutus.com.et.common;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;

/**
 * Created by Admin1 on 17-08-2016.
 */
public class CircleProgressBar {

    CustomProgressDialog dialog;
    private Context mContext;
    boolean isCancelable=true;

    public CircleProgressBar(Context context) {
        mContext = context;

        dialog = new CustomProgressDialog(mContext) {
            @Override
            public void onBackPressed() {
                    if(isCancelable)
                        dialog.cancel();
            }
        };
        dialog.setCancelable(false);
        hide();
    }


    public boolean isShowing() {
        if (dialog.isShowing()) {
            return true;
        } else {
            return false;
        }
    }


    public void setCancelable(boolean isCancelable) {
        dialog.setCancelable(isCancelable);
        this.isCancelable=isCancelable;
    }


    public void show() {
        if (dialog.isShowing()) {
            dialog.hide();
        } else {
            dialog.show();
        }

    }

    public void hide() {
        cancel();
    }
    public void dismiss() {
        dialog.dismiss();
    }
    public void cancel() {
        dialog.cancel();
    }
}
