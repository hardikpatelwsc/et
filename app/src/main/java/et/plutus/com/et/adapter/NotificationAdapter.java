package et.plutus.com.et.adapter;

/**
 * Created by Ketan on 25-01-2016.
 */

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import et.plutus.com.et.BuildConfig;
import et.plutus.com.et.NotificationDetailsActivity;
import et.plutus.com.et.R;
import et.plutus.com.et.common.ItemClickListener;
import et.plutus.com.et.common.Preferences;
import et.plutus.com.et.model.Notification;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.CustomViewHolder> {
    private Context mContext;
    private ArrayList<Notification> notificationList;
    private String str_image = "";
    private String idToBeClicked="";

    public NotificationAdapter(Context context, ArrayList<Notification> newsList) {
        this.mContext = context;
        this.notificationList = newsList;
        if(Preferences.getValue_String(mContext,Preferences.FCM_TYPE).equals("Notification"))
            idToBeClicked=Preferences.getValue_String(mContext,Preferences.FCM_ID);

        Preferences.setValue(mContext, Preferences.FCM_TYPE, "");
        Preferences.setValue(mContext, Preferences.FCM_ID, "");
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_notifications, null);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    @TargetApi(24)
    public void onBindViewHolder(CustomViewHolder customViewHolder, int position) {


        customViewHolder.txt_notification_title.setText(notificationList.get(position).getNotification_title());
        if(BuildConfig.VERSION_CODE<24)
            customViewHolder.txt_notification_desc.setText(Html.fromHtml(notificationList.get(position).getNotification_description()));
        else
            customViewHolder.txt_notification_desc.setText(Html.fromHtml(notificationList.get(position).getNotification_description(),Html.FROM_HTML_MODE_LEGACY));


        customViewHolder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                Intent notification_detail = new Intent(mContext, NotificationDetailsActivity.class);
                notification_detail.putExtra("Title", notificationList.get(position).getNotification_title());
                notification_detail.putExtra("Desc", notificationList.get(position).getNotification_description());
                mContext.startActivity(notification_detail);
            }
        });
        if (!idToBeClicked.equals(""))
        if((notificationList.get(position).getNotification_id().equals(idToBeClicked))) {
            customViewHolder.onClick(customViewHolder.mainRL);
            idToBeClicked="";
            Preferences.setValue(mContext, Preferences.FCM_TYPE, "");
            Preferences.setValue(mContext, Preferences.FCM_ID, "");
        }

//        if (!notificationList.get(position).getNews_image().equals("")) {
//            str_image = notificationList.get(position).getNews_image_path();
////            Glide.with(mContext).load(notificationList.get(position).getNews_image_path())
////                    .into(customViewHolder.img_news);
////            Glide.with(mContext).load(str_image)
////                    .fitCenter()
////                    .crossFade()
////                    .into(customViewHolder.img_news);
//
//            Picasso.with(mContext).load(str_image)
//                    .placeholder(R.drawable.logo).fit().centerCrop()
//                    .into(customViewHolder.img_notification);
//        }

    }

    @Override
    public int getItemCount() {
        return (null != notificationList ? notificationList.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        protected ImageView img_notification;
        protected TextView txt_notification_title, txt_notification_desc;
        private ItemClickListener clickListener;
        private RelativeLayout mainRL;
//        private Cardv

        public CustomViewHolder(View view) {
            super(view);
//            cardView=(CardView) itemView.findViewById(R.id.card_vieww);
            this.mainRL = (RelativeLayout) view.findViewById(R.id.mainRL);
            this.img_notification = (ImageView) view.findViewById(R.id.img_notification);
            this.txt_notification_title = (TextView) view.findViewById(R.id.txt_notification_title);
            this.txt_notification_desc = (TextView) view.findViewById(R.id.txt_notification_desc);
            itemView.setOnLongClickListener(this);
            mainRL.setOnClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getPosition(), true);
//            Log.e("ProductCLickedPosition", "" + getPosition());
//            Snackbar.make(v, "ProductCLickedPosition" + getPosition(), Snackbar.LENGTH_SHORT).show();
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onClick(v, getPosition(), true);
            return true;
        }

    }

    public void animateTo(ArrayList<Notification> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(ArrayList<Notification> newModels) {
        for (int i = notificationList.size() - 1; i >= 0; i--) {
            final Notification model = notificationList.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    public Notification removeItem(int position) {
        final Notification model = notificationList.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    private void applyAndAnimateAdditions(ArrayList<Notification> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Notification model = newModels.get(i);
            if (!notificationList.contains(model)) {
                addItem(i, model);
            }
        }
    }

    public void addItem(int position, Notification model) {
        notificationList.add(position, model);
        notifyItemInserted(position);
    }


    private void applyAndAnimateMovedItems(ArrayList<Notification> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Notification model = newModels.get(toPosition);
            final int fromPosition = notificationList.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Notification model = notificationList.remove(fromPosition);
        notificationList.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }
}
