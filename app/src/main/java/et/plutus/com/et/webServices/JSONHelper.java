package et.plutus.com.et.webServices;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by HARDIKP on 08/19/2016.
 */
public class JSONHelper {
	/**
	 * Create a JsonObject from the KeyValue pairs
	 * 
	 * @param Data
	 *            A HashMap of the String Values
	 * */
	public static JSONObject makeJsonObject (HashMap<String, String> Data) {
		// Final Object from which the JSONString gets generated
		JSONObject Object = new JSONObject();
		try {
			// make the iterations of the HashMap to get the Key-Value pairs and
			// then put them all in to a single Object
			for (HashMap.Entry<String, String> entry : Data.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				try {
					JSONObject Obj = new JSONObject(value);
					Object.put(key, Obj);
				} catch (Exception e) {
					try {
						JSONArray Arr = new JSONArray(value);
						try {
							JSONObject Obj2 = Arr.getJSONObject(0);
							Object.put(key, Arr);
						} catch (Exception e2) {
							Object.put(key, value);
						}
					} catch (Exception e2) {
						Object.put(key, value);
					}
				}

			}
		} catch (Exception e) {
			// if any Exception rises, re initialize the object to nullify the
			// returning Object
			Object = new JSONObject();
		}

		// Convert the Object to the String and then replace some characters to
		// make it a proper JSONString and return
		return Object;
	}

	/**
	 * Create a String Formatted JsonObject from the KeyValue pairs
	 *
	 * @param Data
	 *            A HashMap of the String Values
	 * */
	public static String getJsonObjectString (HashMap<String, String> Data) {
		return convertRequestToProperFormat(makeJsonObject(Data));
	}

	/**
	 * Create a JsonArray from the ArrayList of JSONObjects
	 * 
	 * @param objects
	 *            An ArrayList of String
	 * */
	public static JSONArray makeJsonArray(ArrayList<JSONObject> objects) {
		// Final Array from which the JSONString gets generated
		JSONArray Array = new JSONArray();
		try {

			// Make a loop and put the object values in the Final Array
			for (int i = 0; i < objects.size(); i++) {
				JSONObject obj = new JSONObject(
						convertRequestToProperFormat(objects.get(i)));
				Array.put(obj);
			}
			// if going to catch, it means the input is not an Object, try with
			// the array
		} catch (Exception e) {
			Array = new JSONArray();
			try {
				// Make a loop and put the array values in the Final Array
				for (int i = 0; i < objects.size(); i++) {
					JSONArray arr = new JSONArray(
							convertRequestToProperFormat(objects.get(i)));
					Array.put(arr);
				}
				// if going to catch, it means the input is neither an Object
				// nor an Array, try with the normal String
			} catch (Exception e2) {

				Array = new JSONArray();
				try {

					// Make a loop and put the values in the Final Array as the
					// String itself
					for (int i = 0; i < objects.size(); i++) {
						Array.put(objects.get(i));
					}
					// if going to catch, just return the JsonString as an empty
					// Array
				} catch (Exception e3) {
					// if any Exception rises, re initialize the array to
					// nullify the returning Array
					Array = new JSONArray();
				}
			}
		}
		// Convert the Array to a String and Return, it may contain the data or
		// can be the empty Array JsonString.
		return Array;
	}

	/**
	 * Create a JsonArray from the Array of JSONObjects
	 * 
	 * @param objects
	 *            An array of String
	 * */
	public static JSONArray makeJsonArray(JSONObject[] objects) {
		// Final Array from which the JSONString gets generated
		JSONArray Array = new JSONArray();
		try {

			// Make a loop and put the object values in the Final Array
			for (int i = 0; i < objects.length; i++) {
				JSONObject obj = new JSONObject(
						convertRequestToProperFormat(objects[i]));
				Array.put(obj);
			}
			// if going to catch, it means the input is not an Object, try with
			// the array
		} catch (Exception e) {
			Array = new JSONArray();
			try {
				// Make a loop and put the array values in the Final Array
				for (int i = 0; i < objects.length; i++) {
					JSONArray arr = new JSONArray(
							convertRequestToProperFormat(objects[i]));
					Array.put(arr);
				}
				// if going to catch, it means the input is neither an Object
				// nor an Array, try with the normal String
			} catch (Exception e2) {

				Array = new JSONArray();
				try {

					// Make a loop and put the values in the Final Array as the
					// String itself
					for (int i = 0; i < objects.length; i++) {
						Array.put(objects[i]);
					}
					// if going to catch, just return the JsonString as an empty
					// Array
				} catch (Exception e3) {
					// if any Exception rises, re initialize the array to
					// nullify the returning Array
					Array = new JSONArray();
				}
			}
		}
		// Convert the Array to a String and Return, it may contain the data or
		// can be the empty Array JsonString.
		return Array;
	}

	/**
	 * Convert JSONArray to proper string representation
	 * 
	 * @param jsonArray
	 * 
	 * */
	public static String convertRequestToProperFormat(JSONArray jsonArray) {
		String ProperFormat = String.valueOf(jsonArray).replace("\\\"", "\"");
		return ProperFormat;
	}

	/**
	 * Convert JSONObject to proper string representation
	 * 
	 * @param jsonObject
	 * 
	 * */
	public static String convertRequestToProperFormat(JSONObject jsonObject) {
		String ProperFormat = String.valueOf(jsonObject).replace("\\\"", "\"");
		return ProperFormat;
	}

	/**
	 * Convert Response to proper string representation
	 * 
	 * @param Response
	 *            A StringBuffer that contains the response
	 * 
	 * */
	public static String convertResponseToProperFormat(StringBuffer Response) {
		String FormatedResponse = "";
		try {
			FormatedResponse = Response.toString()
					.substring(1, Response.toString().length() - 1)
					.replace("\\", "");
		} catch (Exception e) {
			// TODO: handle exception
		}

		return FormatedResponse;
	}

}
