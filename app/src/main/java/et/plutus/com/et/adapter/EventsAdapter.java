package et.plutus.com.et.adapter;

/**
 * Created by Ketan on 25-01-2016.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.roomorama.caldroid.Event;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import et.plutus.com.et.EventsDetailsActivity;
import et.plutus.com.et.R;
import et.plutus.com.et.common.ItemClickListener;
import et.plutus.com.et.common.ratioTransformation;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.CustomViewHolder> {
    private Context mContext;
    private ArrayList<Event> eventList;
    private String str_image = "";
    boolean showDates=false;

    public EventsAdapter(Context context, ArrayList<Event> eventList, boolean showDates) {
        this.mContext = context;
        this.eventList = eventList;
        this.showDates = showDates;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_event_fragment, null);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int position) {

        customViewHolder.txt_event_title.setText(eventList.get(position).getEvent_title());
        Log.e("DATE:",convertDateFormat(eventList.get(position).getEvent_start_date()));
        if(showDates) {
            if(eventList.get(position).getEvent_start_date().equals(eventList.get(position).getEvent_end_date()))
                customViewHolder.txt_event_date.setText(convertDateFormat(eventList.get(position).getEvent_start_date()));
            else{
                customViewHolder.txt_event_date.setText(convertDateFormat(eventList.get(position).getEvent_start_date())+" - "+convertDateFormat(eventList.get(position).getEvent_end_date()));
            }
        }
         else
            customViewHolder.txt_event_date.setVisibility(View.GONE);

        customViewHolder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                Intent events = new Intent(mContext, EventsDetailsActivity.class);
                events.putExtra("event_id", eventList.get(position).getEvent_id());
                mContext.startActivity(events);
            }
        });

        if (!eventList.get(position).getEvent_image_name().equals("")) {
            str_image = eventList.get(position).getEvent_image_path();

            Picasso.with(mContext).load(str_image)
                    .placeholder(R.drawable.event_placeholder)
                    .transform(new ratioTransformation(customViewHolder.img_event))
                    .into(customViewHolder.img_event);
        }

    }

    @Override
    public int getItemCount() {
        return (null != eventList ? eventList.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        protected ImageView img_event;
        protected TextView txt_event_title;
        protected TextView txt_event_date;
        private ItemClickListener clickListener;
        private FrameLayout mainFL;

        public CustomViewHolder(View view) {
            super(view);
            this.mainFL=(FrameLayout) view.findViewById(R.id.mainFL);
            this.img_event = (ImageView) view.findViewById(R.id.img_event);
            this.txt_event_title = (TextView) view.findViewById(R.id.txt_event_title);
            this.txt_event_date = (TextView) view.findViewById(R.id.txt_event_date);
            itemView.setOnLongClickListener(this);
            mainFL.setOnClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getPosition(), true);
            Log.e("ProductCLickedPosition", "" + getPosition());
//            Snackbar.make(v, "ProductCLickedPosition" + getPosition(), Snackbar.LENGTH_SHORT).show();
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onClick(v, getPosition(), true);
            return true;
        }

    }
    public void setFilter(List<Event> countryModels) {
        eventList = new ArrayList<>();
        eventList.addAll(countryModels);
        notifyDataSetChanged();
    }

    public static String convertDateFormat(String date) {
        String Date = "";
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat output = new SimpleDateFormat("d'## 'MMM yyyy");
            SimpleDateFormat formatDayOfMonth  = new SimpleDateFormat("d");

            Date d = sdf.parse(date);
            Date = output.format(d);

            int day = Integer.parseInt(formatDayOfMonth.format(d));
            Date = Date.toUpperCase().replace("##",suffixes[day]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Date;
    }
    static String[] suffixes =
    ///    0     1     2     3     4     5     6     7     8     9
        { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                //    10    11    12    13    14    15    16    17    18    19
                    "th", "th", "th", "th", "th", "th", "th", "th", "th", "th",
                //    20    21    22    23    24    25    26    27    28    29
                    "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                //    30    31
                    "th", "st" };
}
