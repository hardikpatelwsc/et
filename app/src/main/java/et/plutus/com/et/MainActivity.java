package et.plutus.com.et;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import et.plutus.com.et.common.Logger;
import et.plutus.com.et.common.Preferences;
import et.plutus.com.et.fragment.AboutUsFragment;
import et.plutus.com.et.fragment.EventsFragment;
import et.plutus.com.et.fragment.NewsFragment;
import et.plutus.com.et.fragment.NotificationFragment;
import et.plutus.com.et.webServices.API;
import et.plutus.com.et.webServices.AsyncTaskListener;
import et.plutus.com.et.webServices.JSONHelper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int PERMISSION_REQUEST_CODE = 11;
    Toolbar toolbar;
    ImageView img_show_logo;
    TextView tv_show_name;
    private Context mContext = MainActivity.this;
    private boolean doubleBackToExitPressedOnce = false;
    private LinearLayout ll_poweredBy;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        initComponents();
        askAllPermission();

        if (SplashActivity.flag_push) {
            if (Preferences.getValue_String(mContext, Preferences.FCM_TYPE).equalsIgnoreCase("Notification")) {
                displayView(2);
                navigationView.getMenu().getItem(2).setChecked(true);
                SplashActivity.flag_push = false;
            } else {
                displayView(0);
                navigationView.getMenu().getItem(0).setChecked(true);
                Intent news = new Intent(mContext, NewsDetailsActivity.class);
                String news_id = Preferences.getValue_String(mContext, Preferences.FCM_ID);
                Log.e("News_ID", "" + news_id);
                news.putExtra("newsfeed_id", news_id);
                mContext.startActivity(news);
                Preferences.setValue(mContext, Preferences.FCM_TYPE, "");
                Preferences.setValue(mContext, Preferences.FCM_ID, "");
            }
        } else {
            displayView(0);
            navigationView.getMenu().getItem(0).setChecked(true);
        }

        sendDeviceInfo();
        checkAndShowAppIntro();
    }

    private void initComponents() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ll_poweredBy = (LinearLayout) findViewById(R.id.ll_poweredBy);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        toolbar.setTitle("News");
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        img_show_logo = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.img_show_logo);
        tv_show_name = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tv_show_name);

        ll_poweredBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://www.eventrooper.com/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

    }

    private void checkAndShowAppIntro() {
        if (!Preferences.getValue_Boolean(mContext, Preferences.isIntro, false)) {
            Intent intro = new Intent(mContext, AppIntroductionActivity.class);
            startActivity(intro);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else {
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (EventsFragment.isInfoVisible) {
                EventsFragment.onBackPressed();
                return;
            }
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    //checks and asks for all permission
    private void askAllPermission() {
        SharedPreferences prefs = getSharedPreferences(Preferences.PREFS_PERMISSIONS, MODE_PRIVATE);
        boolean permissionAsked = prefs.getBoolean(Preferences.PREF_PERMISSION_ASKED, false);
        if (!permissionAsked) {
            ArrayList<String> permissions = checkAllPermissions();
            if (permissions.size() > 0) {
                String[] strArray = new String[permissions.size()];
                strArray = permissions.toArray(strArray);
                ActivityCompat.requestPermissions(this, strArray, PERMISSION_REQUEST_CODE);
            }
        }
    }

    //checks all permission
    private ArrayList<String> checkAllPermissions() {
        ArrayList<String> strings = new ArrayList<>();
        int resultStorage = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (resultStorage != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        int resultLocation = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (resultLocation != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        return strings;
    }


    //on permission result
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                try {
                    Logger.e("permissions : " + Arrays.toString(permissions));
                    Logger.e("grantResults : " + Arrays.toString(grantResults));

                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Logger.e("Permission granted. Now you can access data.");
                    } else {
                        Logger.e("Permission Denied. You can not access data.");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            if(EventsFragment.isInfoVisible)
                EventsFragment.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_news) {
            displayView(0);
        } else if (id == R.id.nav_events) {
            displayView(1);

        }
//        else if (id == R.id.nav_exhibitors) {
//            displayView(2);
//
//        }
        else if (id == R.id.nav_notifications) {
            displayView(2);

        } else if (id == R.id.nav_aboutus) {
            displayView(3);

        }
//        else if (id == R.id.nav_login) {
////            displayView(0);
//            Toast.makeText(mContext, "Please Login into Application", Toast.LENGTH_SHORT).show();
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /*Method for Displaying Views for fragments using positions*/
    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new NewsFragment();
                toolbar.setTitle("News");
                break;
            case 1:
                fragment = new EventsFragment();
                toolbar.setTitle("Event");
                break;
//            case 2:
//                fragment = new ExhibitorsFragment();
//                toolbar.setTitle("Exhibitors");
//                break;
            case 2:
                fragment = new NotificationFragment();
                toolbar.setTitle("Notification");
                break;
            case 3:
                fragment = new AboutUsFragment();
                toolbar.setTitle("AboutUs");
                break;
            default:
                break;
        }

 /*Method for checking fragment NULL or NOT*/
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment).commit();

        } else {
            // error in creating fragment
            Logger.e("MainActivity_Old", "Error in creating fragment");
        }
    }


    public void getProfile() {
        //Make Post Params
        HashMap<String, String> params = new HashMap<>();
        String postParameters = JSONHelper.getJsonObjectString(params);
        ////////////////////////
        API api = new API(mContext, true, new AsyncTaskListener() {
            @Override
            public void onFailure(String FailureCause) {
                Log.e("Failure: ", FailureCause + "");
            }

            @Override
            public void responseExtractLogic(String response) {
                try {
                    JSONObject obj_main = new JSONObject(response);
                    Logger.e("Response" + response);
                    JSONObject obj_data = obj_main.optJSONObject("data");
                    JSONObject obj_show = obj_data.optJSONObject("Show");
                    String image_path = obj_show.optString("show_image_full_path");
                    String name = obj_show.optString("name");
                    tv_show_name.setText(name);
                    Log.e("Image_logo", "" + image_path);
                    Picasso.with(mContext).load(image_path)
                            .placeholder(R.drawable.event_placeholder).fit().centerCrop()
                            .into(img_show_logo);
//                    Preferences.setValue(mContext, Preferences.IMAGE_LOGO, image_path);
                } catch (Exception E) {
                    onFailure(API.MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                }
            }
        }, false);
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, API.API_ABOUT_US, postParameters);


    }

// Make API call to send FCM Token to the server
    public void sendDeviceInfo() {
        //Make Post Params
        HashMap<String, String> params = new HashMap<>();
        params.put("device_token", Preferences.getValue_String(mContext, Preferences.FCM_Token));
        params.put("platform", "Android");
        String postParameters = JSONHelper.getJsonObjectString(params);
        ////////////////////////
        API api = new API(mContext, true, new AsyncTaskListener() {
            @Override
            public void onFailure(String FailureCause) {
                Log.e("Failure: ", FailureCause + "");
            }

            @Override
            public void responseExtractLogic(String response) {
                try {
                    JSONObject obj_main = new JSONObject(response);
                    Logger.e("Response_Device_Info" + response);

                } catch (Exception E) {
                    onFailure(API.MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                }
                getProfile();
            }
        }, false);
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, API.API_GET_DEVICE_INFO, postParameters);


    }
}
