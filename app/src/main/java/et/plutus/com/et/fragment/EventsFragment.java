package et.plutus.com.et.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidHandler;
import com.roomorama.caldroid.CaldroidListener;
import com.roomorama.caldroid.Event;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import et.plutus.com.et.EventListingDateWiseActivity;
import et.plutus.com.et.R;
import et.plutus.com.et.adapter.EventsAdapter;
import et.plutus.com.et.caldroid.CaldroidSampleCustomFragment;
import et.plutus.com.et.common.AppConstants;
import et.plutus.com.et.common.Logger;
import et.plutus.com.et.common.MyCustomLayoutManager;
import et.plutus.com.et.webServices.API;
import et.plutus.com.et.webServices.AsyncTaskListener;
import et.plutus.com.et.webServices.JSONHelper;
import io.codetail.widget.RevealFrameLayout;

/**
 * Created by Ketan on 03-05-2016.
 */
public class EventsFragment extends Fragment implements SearchView.OnQueryTextListener, View.OnClickListener {

    public static Context mContext;
    public static MyCustomLayoutManager mLayoutManager;
    public static CaldroidHandler calHandler=CaldroidHandler.getInstance();
    public static boolean isInfoVisible = false;
    private static RevealFrameLayout fl_info;
    private static TableLayout tl_info;
    // For Information Popup
    private static int cx, cy;
    private static float radius;
    private static ImageView iv_info, iv_cancel;
    Handler handler = new Handler();
    LinearLayout CalendarContainer;
    private RelativeLayout mRoot;
    private RecyclerView recycler_view_events;
    private EventsAdapter adapter_events;
    private TextView tv_info_title;
    private LinearLayout ll_info_bubble;

    private boolean FirstTime = true;


    public static void onBackPressed() {
        hideInfoPopup();
    }


    private static void hideInfoPopup() {
        animateInfoPopup();
    }


    private static void animateInfoPopup() {

        cx = ((iv_info.getRight() + iv_info.getLeft()) / 2);
        cy = ((iv_info.getTop() + iv_info.getBottom()) / 2);
        radius = pythagoras(fl_info.getWidth(), fl_info.getHeight());

        if (isInfoVisible) {
            fl_info.setEnabled(false);
            iv_cancel.setEnabled(false);
            iv_info.setEnabled(false);
            Animator anim = android.view.ViewAnimationUtils.createCircularReveal(fl_info, cx, cy, radius, 0);
            anim.setDuration(700);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    fl_info.setVisibility(View.GONE);
                    fl_info.setEnabled(true);
                    iv_cancel.setEnabled(true);
                    iv_info.setEnabled(true);
                    emptyInfoTable();
                    isInfoVisible = false;
                }
            });
            anim.start();
        } else {
            fl_info.setEnabled(false);
            iv_cancel.setEnabled(false);
            iv_info.setEnabled(false);
            Animator anim = android.view.ViewAnimationUtils.createCircularReveal(fl_info, cx, cy, 0, radius);
            fl_info.setVisibility(View.VISIBLE);
            fl_info.setEnabled(true);
            iv_cancel.setEnabled(true);
            iv_info.setEnabled(true);
            anim.setDuration(700);
            anim.start();
            isInfoVisible = true;
        }
    }

    private static float pythagoras(double x, double y) {
        return (float) (Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
    }

    private static void emptyInfoTable() {
        tl_info.removeAllViews();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle bundle) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_events, container, false);
        mContext = getActivity();
        FirstTime = true;

        initViews(view);
//        getAds();
//        getTimelyAdd.start();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (FirstTime) {
            FirstTime = false;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    initCalendar();
                }
            });
        }
    }

    private void initCalendar() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                CaldroidFragment caldroidFragment = new CaldroidSampleCustomFragment();
                Bundle args = new Bundle();
                Calendar cal = Calendar.getInstance();
                args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
                args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
                args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
                args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, false);
                args.putInt(CaldroidFragment.START_DAY_OF_WEEK,
                        CaldroidFragment.MONDAY); // Tuesday
                caldroidFragment.setArguments(args);


                // Attach to the activity
                FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                t.replace(CalendarContainer.getId(), caldroidFragment);
                t.commit();
                calHandler.setFragment(mContext,caldroidFragment);


                // Setup listener
                final CaldroidListener listener = new CaldroidListener() {

                    @Override
                    public void onSelectDate(Date date, View view) {
                        if (calHandler.EventDatesString.contains(AppConstants.formatter.format(date))) {
//                            Toast.makeText(mContext, AppConstants.formatter.format(date) + " Date Selected", Toast.LENGTH_LONG).show();
                            Intent listing = new Intent(mContext, EventListingDateWiseActivity.class);
                            listing.putExtra("EventDate", AppConstants.formatter_api.format(date));
                            mContext.startActivity(listing);
                        }
//                    Toast.makeText(mContext, AppConstants.formatter.format(date) + " Total Updates: " + EventDateOrders.get(EventDatesString.indexOf(AppConstants.formatter.format(date))).size(),
//                            Toast.LENGTH_SHORT).show();
//                ArrayList<Integer> data = EventDateOrders.get(EventDatesString.indexOf(AppConstants.formatter.format(date)));
//                for (int j = 0; j < data.size(); j++) {
//                    Log.e("ID on", "Clicked Date is: " + data.get(j));
//                }
//                Global.Dbobject.openDataBase();
//                Cursor cursor_count = Global.Dbobject
//                        .selectquery("SELECT Count(*) FROM ScheduleDetails WHERE EventDateString='"+AppConstants.formatter.format(date)+"'");
//                cursor_count.moveToNext();
//                Log.e("Data Count", "" + cursor_count.getInt(0));
//                Global.Dbobject.close();

                    }

                    @Override
                    public void onChangeMonth(final int month, final int year) {
                        Calendar calFrom, calTo;
                        calTo = Calendar.getInstance();
                        calFrom = Calendar.getInstance();
                        calTo.set(year, month - 1, 1);
                        calTo.set(Calendar.DAY_OF_MONTH, calTo.getActualMaximum(Calendar.DAY_OF_MONTH));
                        calFrom.set(year, month - 1, 1);
                        getEventsList(AppConstants.formatter_api.format(calFrom.getTime()), AppConstants.formatter_api.format(calTo.getTime()));
//                String text = "month: " + month + " year: " + year;
//                Toast.makeText(mContext, "Month End Date: " + AppConstants.formatter_api.format(calTo.getTime()),
//                        Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onLongClickDate(Date date, View view) {
//                Toast.makeText(mContext,
//                        "Long click " + AppConstants.formatter.format(date),
//                        Toast.LENGTH_SHORT).show();
                        if (calHandler.EventDatesString.contains(AppConstants.formatter.format(date))) {
//                            Toast.makeText(mContext, AppConstants.formatter.format(date) + " Date Selected", Toast.LENGTH_LONG).show();
                            showInfoPopup(1, date);
                        }
                    }

                    @Override
                    public void onCaldroidViewCreated() {
                        if (calHandler.caldroidFragment.getLeftArrowButton() != null) {
//                    Toast.makeText(mContext,
//                            "Caldroid view is created", Toast.LENGTH_SHORT)
//                            .show();
                        }
                    }

                };

                // Setup Caldroid
                calHandler.caldroidFragment.setCaldroidListener(listener);
            }
        }).start();

    }

    /*private void clearCalendar() {
        if (calHandler.caldroidFragment != null) {
            iv_info.setVisibility(View.GONE);
            calHandler.event_list_model.clear();
            calHandler.event_list_multipleDays.clear();
            calHandler.LongEvents.clear();
            calHandler.LongEventsType.clear();
            calHandler.LongEventsColor.clear();
            calHandler.caldroidFragment.setEvents(calHandler.EventDates, calHandler.Events, calHandler.LongEvents, calHandler.LongEventsType, calHandler.LongEventsColor);
            calHandler.caldroidFragment.refreshView();
        }
    }*/

    /*private void setCustomResourceForDates(String response) {
        if (calHandler.caldroidFragment != null) {
            calHandler.event_list_model.clear();
            calHandler.event_list_multipleDays.clear();
            calHandler.LongEvents.clear();
            calHandler.LongEventsType.clear();
            calHandler.LongEventsColor.clear();
            getEventDates(response);
            calHandler.caldroidFragment.setEvents(calHandler.EventDates, calHandler.Events, calHandler.LongEvents, calHandler.LongEventsType, calHandler.LongEventsColor);
            calHandler.caldroidFragment.refreshView();
        }
    }*/


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FirstTime = true;
    }

    public void setAdapter() {

        adapter_events = new EventsAdapter(mContext, calHandler.event_list_model, true);
        recycler_view_events.setAdapter(adapter_events);

    }

    // Initialize View for the Fragment
    void initViews(View view) {
        mRoot = (RelativeLayout) view.findViewById(R.id.mRoot);
        recycler_view_events = (RecyclerView) view.findViewById(R.id.recycler_view_events);
        mLayoutManager = new MyCustomLayoutManager(mContext);
        recycler_view_events.setLayoutManager(mLayoutManager);
        recycler_view_events.setHasFixedSize(true);
        recycler_view_events.addItemDecoration(new HorizontalDividerItemDecoration.Builder(mContext).build());
        CalendarContainer = (LinearLayout) view.findViewById(R.id.CalendarContainer);
        setHasOptionsMenu(true);

        iv_info = (ImageView) view.findViewById(R.id.iv_info);
        iv_cancel = (ImageView) view.findViewById(R.id.iv_cancel);
        fl_info = (RevealFrameLayout) view.findViewById(R.id.fl_info);
        tv_info_title = (TextView) view.findViewById(R.id.tv_info_title);
        tl_info = (TableLayout) view.findViewById(R.id.tl_info);
        ll_info_bubble = (LinearLayout) view.findViewById(R.id.ll_info_bubble);

        iv_info.setOnClickListener(this);
        iv_cancel.setOnClickListener(this);
        ll_info_bubble.setOnClickListener(this);
        fl_info.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_info:
                showInfoPopup(0, new Date());
                break;
            case R.id.iv_cancel:
                hideInfoPopup();
                break;
            case R.id.ll_info_bubble:
                break;
            case R.id.fl_info:
                hideInfoPopup();
                break;
        }

    }
// Show Information popup depending onthe type whether it is a color info popup or details popup for any particular date
    private void showInfoPopup(int popupType, Date date) {
        int colorInfoPopup = 0; // else it would be the DateDetail popup
        if (popupType == colorInfoPopup) {
            fillColorInfoTable();
        } else {
            fillDetailInfoTable(date);
        }
    }

    // Filter events and show different color information for whole month
    private void fillColorInfoTable() {
        tv_info_title.setText("Events Information");

        for (int i = 0; i < calHandler.event_list_model.size(); i++) {
            if (!calHandler.event_list_model.get(i).getEvent_start_date().equals(calHandler.event_list_model.get(i).getEvent_end_date())) {

                TableRow tr = (TableRow) LayoutInflater.from(getActivity()).inflate(R.layout.row_color_info_popup, null, false);
                TextView tv_info_detail = (TextView) tr.findViewById(R.id.tv_info_detail);
                TextView color = (TextView) tr.findViewById(R.id.color);

                tv_info_detail.setText(convertDateFormat_APItoSIMPLE(calHandler.event_list_model.get(i).getEvent_start_date()) + " - " + convertDateFormat_APItoSIMPLE(calHandler.event_list_model.get(i).getEvent_end_date()));
                AppConstants.setViewColor(color, CaldroidFragment.COLORS[tl_info.getChildCount()]);

                tl_info.addView(tr);
            }
        }
        animateInfoPopup();
    }

    private String convertDateFormat_APItoSIMPLE(String Date) {
        String[] date_ary = Date.split("-");
        int YEAR = Integer.parseInt(date_ary[0]);
        int MONTH = Integer.parseInt(date_ary[1]) - 1;
        int DATE = Integer.parseInt(date_ary[2]);
        Calendar cal = Calendar.getInstance();
        cal.set(YEAR, MONTH, DATE);
        return AppConstants.formatter.format(cal.getTime());
    }

    private void fillDetailInfoTable(Date date) {

        tv_info_title.setText("Events on " + AppConstants.formatter.format(date));

        String EventDateExtra = AppConstants.formatter_api.format(date);

        for (int i = 0; i < calHandler.event_list_model.size(); i++) {
            Event event = calHandler.event_list_model.get(i);
            String StartDate = event.getEvent_start_date();
            String EndDate = event.getEvent_end_date();
            Date SDate = null, EDate = null;
            try {
                SDate = AppConstants.formatter_api.parse(StartDate);
                EDate = AppConstants.formatter_api.parse(EndDate);
            } catch (Exception E) {
            }
            if (AppConstants.dateBetween(date, SDate, EDate) || EventDateExtra.equals(StartDate) || EventDateExtra.equals(EndDate)) {
                TableRow tr = (TableRow) LayoutInflater.from(getActivity()).inflate(R.layout.row_date_info_popup, null, false);
                TextView tv_info_detail = (TextView) tr.findViewById(R.id.tv_info_detail);
                TextView number = (TextView) tr.findViewById(R.id.number);
                number.setText((tl_info.getChildCount() + 1) + ". ");
                tv_info_detail.setText(event.getEvent_title());

                tl_info.addView(tr);
            }
        }
        animateInfoPopup();
    }

    // Get Events from server on a particular time of period (in this case 1 month)
    public void getEventsList(String start_date, String end_date) {
        //Make Post Params
        HashMap<String, String> params = new HashMap<>();
        Logger.e("START_DATE", start_date + "##");
        Logger.e("END_DATE", end_date + "##");
        if (!TextUtils.isEmpty(start_date))
            params.put("start_date", start_date);
        if (!TextUtils.isEmpty(end_date))
            params.put("end_date", end_date);
//        params.put("end_date", date_params.get(date_params.size()-1));
        String postParameters = JSONHelper.getJsonObjectString(params);
        ////////////////////////
        API api = new API(mContext, true, new AsyncTaskListener() {
            @Override
            public void onFailure(String FailureCause) {
                Log.e("Failure: ", FailureCause + "");
            }

            @Override
            public void responseExtractLogic(String response) {
                try {
                    JSONObject obj_main = new JSONObject(response);
                    Logger.e("Response" + response);
                    if (obj_main.optInt("status") == 1) {
                        setAdapter();
                        calHandler.setCustomResourceForDates(response,mRoot,iv_info);
                    } else {
                        if (obj_main.optString("message").contains("not exist")) {
                            Snackbar.make(mRoot, obj_main.optString("message").replace("Event does not exist", "No events found, please check back later for updates"), Snackbar.LENGTH_LONG).show();
                            calHandler.clearCalendar();
                            iv_info.setVisibility(View.GONE);
                        }
                    }
                } catch (Exception E) {
                    onFailure(API.MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                }
            }
        });
        api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, API.API_GET_EVENTS, postParameters);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() > 0) {
            CalendarContainer.setVisibility(View.GONE);
            recycler_view_events.setVisibility(View.VISIBLE);
        } else {
            CalendarContainer.setVisibility(View.VISIBLE);
            recycler_view_events.setVisibility(View.GONE);
        }
        final List<Event> filteredModelList = filter(calHandler.event_list_model, newText);
        adapter_events.setFilter(filteredModelList);
        return true;
    }

    private List<Event> filter(List<Event> models, String query) {
        query = query.toLowerCase();

        final List<Event> filteredModelList = new ArrayList<>();
        for (Event model : models) {
            final String text = model.getEvent_title().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        final ImageView mCloseButton = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        adapter_events.setFilter(calHandler.event_list_model);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        return true; // Return true to expand action view
                    }
                });

        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView.setQuery("", false);
                //Collapse the action view
                searchView.onActionViewCollapsed();
                //Collapse the search widget
                item.collapseActionView();
            }
        });
    }
}
