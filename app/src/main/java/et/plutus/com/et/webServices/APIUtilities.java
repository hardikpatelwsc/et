package et.plutus.com.et.webServices;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HARDIKP on 08/19/2016.
 */
public class APIUtilities {

    /**
     * For showing AlertDialog from anywhere with only one button Visible.
     *
     * @param Title
     *            Title to be shown
     * @param Message
     *            Message to be shown
     * @param ButtonText
     *            Text for Button, if null or emptyString, it will show OK as
     *            text
     * @param ifCancelable
     *            whether the AlertDialog is Cancelable or not
     * @param context
     *            Activity Context
     *
     * */
    public static void ShowAlertWithOneButton(String Title, String Message,
                                              String ButtonText, boolean ifCancelable
            , Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        if (Title != null)
            if (!Title.equals(""))
                alertDialogBuilder.setTitle(Title);
        alertDialogBuilder
                .setMessage(Message)
                .setCancelable(ifCancelable)
                .setPositiveButton(
                        (ButtonText == null) ? API.OK
                                : ButtonText.equals("") ? API.OK
                                : ButtonText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    /**
     * For showing AlertDialog from anywhere with only one button Visible.
     *
     * @param Title
     *            Title to be shown
     * @param Message
     *            Message to be shown
     * @param ButtonText
     *            Text for Button, if null or emptyString, it will show OK as
     *            text
     * @param ifCancelable
     *            whether the AlertDialog is Cancelable or not
     * @param context
     *            Activity Context
     * @param ClickListener
     *            Action to be performed on button Click
     *
     * */
    public static void ShowAlertWithOneButton(String Title, String Message,
                                              String ButtonText, boolean ifCancelable, Context context,
                                              final SetOnClickListener ClickListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        if (Title != null)
            if (!Title.equals(""))
                alertDialogBuilder.setTitle(Title);
        alertDialogBuilder
                .setMessage(Message)
                .setCancelable(ifCancelable)
                .setPositiveButton(
                        (ButtonText == null) ? API.OK
                                : ButtonText.equals("") ? API.OK
                                : ButtonText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                ClickListener.onPositiveButtonClick();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * For Preparing the URLEncoded DataString used in the POST Requests
     *
     * @param params        <strong>HashMap<String, String></strong> a key-value pair of
     *                      the String type.
     * @param convertToUTF8 pass true if want to Convert the attaching parameters to UTF8
     *                      characterSet
     */
    public static String getPostDataString(HashMap<String, String> params,
                                           boolean convertToUTF8) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append((convertToUTF8) ? (URLEncoder.encode(entry.getKey(),
                    "UTF-8")) : (entry.getKey()));
            result.append("=");
            result.append((convertToUTF8) ? (URLEncoder.encode(
                    entry.getValue(), "UTF-8")) : (entry.getValue()));
        }

        return result.toString();
    }
}
