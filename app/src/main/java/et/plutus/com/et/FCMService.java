package et.plutus.com.et;


import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import et.plutus.com.et.common.Logger;
import et.plutus.com.et.common.Preferences;

/**
 * Created by Ketan on 21-06-2016.
 */
public class FCMService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    public static long NOTIFICATION_ID = 0;
    public static NotificationManager notificationManager;
    JSONObject obj_notification;
    String[] Notifications;
    Context mContext;
    JSONObject obj_response;
    private String Title = "";
    private String Event_name = "";
    private String Category_name = "";
    private String Order_Id = "";
    private String notification_type = "";
    private String notification_id = "";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            mContext = getApplicationContext();
            // TODO(developer): Handle FCM messages here.
            // If the application is in the foreground handle both data and notification messages here.
            // Also if you intend on generating your own notifications as a result of a received FCM
            // message, here is where that should be initiated. See sendNotification method below.

            try {
                Logger.e(TAG, "Response Message Data From Server: " + remoteMessage.getData());
                obj_response = new JSONObject(remoteMessage.getData());
                notification_type = obj_response.optString("type");
                notification_id = obj_response.optString("nid");
                Logger.e(TAG, "Title: " + obj_response.getString("title"));

                sendNotification(obj_response.getString("title"));
            } catch (Exception E) {
                Logger.e(TAG, "Response Message Data From Server: " + E.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {

        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder notificationBuilder = null;
        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("Came_From", "FCM_Service");
        intent.putExtra("notification_type", notification_type);
        intent.putExtra("notification_id", notification_id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        notificationBuilder = new NotificationCompat.Builder(this)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody).setAutoCancel(true)
                .setContentIntent(pendingIntent);

        long time = System.currentTimeMillis();
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
//        notificationManager.notify(((int) time) /* ID of notification */, notificationBuilder.build());
    }
/* Save the messages in array */

    private void saveArray(String[] array/* , Context mContext */) {
        SharedPreferences prefs = /* mContext. */getSharedPreferences(
                Preferences.Valuepad_PREFRENCE, 0);
        SharedPreferences.Editor editor = prefs.edit();
        Map<String, ?> keys = prefs.getAll();

        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            Logger.d("map values", entry.getKey() + ": "
                    + entry.getValue().toString());
            if (entry.getKey().contains(Preferences.NotificationArrayName + "_"))
                editor.remove(entry.getKey());
        }
        editor.putInt(Preferences.NotificationArrayName + "_size", array.length);
        for (int i = 0; i < array.length; i++)
            editor.putString(Preferences.NotificationArrayName + "_" + i, array[i]);
        Logger.e("Save Succeed?", editor.commit() + "");
    }

    /*Check if the app is in foreground of background*/
    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

}
