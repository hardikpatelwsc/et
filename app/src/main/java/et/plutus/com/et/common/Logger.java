package et.plutus.com.et.common;

import android.util.Log;

/**
 * Created by HARDIKP on 08/05/2016.
 */
public class Logger {
    // Normal
    public static void d(String TAG, String MSG) {

        Log.d(TAG, MSG);

    }

    public static void e(String TAG, String MSG) {

        Log.e(TAG, MSG);

    }

    public static void i(String TAG, String MSG) {

        Log.i(TAG, MSG);

    }

    public static void v(String TAG, String MSG) {

        Log.v(TAG, MSG);

    }

    public static void w(String TAG, String MSG) {

        Log.w(TAG, MSG);

    }

    // With Auto TAG
    public static void d(String MSG) {

        Log.d("LOGGER-D", MSG);

    }

    public static void e(String MSG) {

        Log.e("LOGGER-E", MSG);

    }

    public static void i(String MSG) {

        Log.i("LOGGER-I", MSG);

    }

    public static void v(String MSG) {

        Log.v("LOGGER-V", MSG);

    }

    public static void w(String MSG) {

        Log.w("LOGGER-W", MSG);

    }


    // With Throwable
    public static void d(String TAG, String MSG, Throwable TR) {

        Log.d(TAG, MSG, TR);

    }

    public static void e(String TAG, String MSG, Throwable TR) {

        Log.e(TAG, MSG, TR);

    }

    public static void i(String TAG, String MSG, Throwable TR) {

        Log.i(TAG, MSG, TR);

    }

    public static void v(String TAG, String MSG, Throwable TR) {

        Log.v(TAG, MSG, TR);

    }

    public static void w(String TAG, String MSG, Throwable TR) {

        Log.w(TAG, MSG, TR);

    }
}
