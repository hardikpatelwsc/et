package et.plutus.com.et.webServices;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import et.plutus.com.et.common.CircleProgressBar;

/**
 * Created by HARDIKP on 08/19/2016.
 */
public class API extends AsyncTask<String, Void, String> {
    public static final int WAIT_TIME = 20000;
    public static final String REQUEST_METHOD_POST = "POST"; // POST_type_of_HTTP_Request_Method
    public static final String REQUEST_METHOD_GET = "GET"; // POST_type_of_HTTP_Request_Method
    public static final String RESPONSE_TIMEOUT = "RESPONSE_TIMEOUT";
    public static final String RESPONSE_UNWANTED = "RESPONSE_UNWANTED";
    public static final String RESPONSE_NOINTERNET = "RESPONSE_NOINTERNET";
    public static final String OK = "OK";
    public static final String TITLE_UNWANTED = "Sorry…";
    public static final String TITLE_TIMEOUT = "Sorry…";
    public static final String PD_PLEASE_WAIT = "Please Wait…";
    public static final String MESSAGE_UNWANTED = "Some error has been occured, please contact the authority…";
    public static final String MESSAGE_TIMEOUT = "Server taking to much time to respond…";
    public static final String MESSAGE_NO_INTERNET = "No internet access";

    public static final String BASE_URL = "http://www.evmgmt.i3brains.com/api";
    public static final String API_URL = BASE_URL;
    public static final String API_LOGIN = API_URL + "/login_api";
    public static final String API_LOGOUT = API_URL + "/logout_api";
    public static final String API_GET_NEWS = API_URL + "/get_newsfeeds";
    public static final String API_GET_EVENTS = API_URL + "/get_events";
    public static final String API_GET_EXHIBITOR = API_URL + "/get_exhibitor";
    public static final String API_ABOUT_US = API_URL + "/getProfile";
    public static final String API_GET_ADS = API_URL + "/getAds";
    public static final String API_VIEW_ADS = API_URL + "/viewAds";
    public static final String API_GET_NOTIFICATIONS = API_URL + "/getNotifications";
    public static final String API_GET_DEVICE_INFO = API_URL + "/getDeviceData";
    public static final String SHOW_CODE = "gmdc";
    private Context mContext;
    private AsyncTaskListener mAsyncTaskListener;
    private boolean showProgressDialog = true;
    private boolean showDefaultAlerts;
    private CircleProgressBar oProgressDialog = null;

    public API(Context mContext, boolean showDefaultAlerts, AsyncTaskListener mAsyncTaskListener, boolean showProgressDialog) {
        this.mContext = mContext;
        this.showProgressDialog = showProgressDialog;
        this.showDefaultAlerts = showDefaultAlerts;
        this.mAsyncTaskListener = mAsyncTaskListener;
    }

    public API(Context mContext, boolean showDefaultAlerts, AsyncTaskListener mAsyncTaskListener) {
        this.mContext = mContext;
        this.showDefaultAlerts = showDefaultAlerts;
        this.mAsyncTaskListener = mAsyncTaskListener;
    }

    public static String performPostCall(String requestURL,
                                         String postDataParams) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(API.WAIT_TIME);
            conn.setConnectTimeout(API.WAIT_TIME);
            conn.setRequestMethod(API.REQUEST_METHOD_POST);
            conn.setRequestProperty("Content-Type",
                    "application/json; charset=UTF-8");
            conn.setRequestProperty("x-show-id", SHOW_CODE);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                    os, "UTF-8"));
            writer.write(postDataParams);

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = API.RESPONSE_UNWANTED + "#" + responseCode
                        + "#" + conn.getResponseMessage();
            }
        } catch (SocketTimeoutException e) {
            response = API.RESPONSE_TIMEOUT;
        } catch (Exception e) {
            response = API.RESPONSE_UNWANTED;
        }
        // response = ConvertResponseToProperFormat(response);
        return response;
    }

    public static boolean isOnline() {
        // // Old Solution
//        Runtime runtime = Runtime.getRuntime();
//        try {
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
//            int exitValue = ipProcess.waitFor();
//            return (exitValue == 0);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        return false;

        // // New Solution
        boolean success = false;
        try {
            URL url = new URL("https://google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    protected String doInBackground(String... strings) {
        String url, postParameters;
        if (isOnline()) {
            url = strings[0];
            postParameters = strings[1];
            Log.e("URL: ", url);
            Log.e("postParameters: ", postParameters);
            String response = "";// a String to save response
            try {
                response = API.performPostCall(url, postParameters);
            } catch (Exception e) {
                response = API.RESPONSE_UNWANTED;
            }
            return response;
        } else
            return API.RESPONSE_NOINTERNET;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        oProgressDialog = new CircleProgressBar(mContext);
//        oProgressDialog.setMessage(PD_PLEASE_WAIT/*getResources().getString(R.string.PD_PLEASE_WAIT)*/);
        oProgressDialog.setCancelable(false);
        try {
            if (showProgressDialog)
                oProgressDialog.show();
        } catch (Exception e) {
        }

    }

    @Override
    protected void onProgressUpdate(Void... unsued) {

    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        try {
            if (showProgressDialog)
                if (oProgressDialog.isShowing())
                    oProgressDialog.dismiss();
        } catch (Exception e) {
        }

        if (!response.startsWith(API.RESPONSE_NOINTERNET)) {
            if (!response.startsWith(API.RESPONSE_TIMEOUT)) {
                if (!response.startsWith(API.RESPONSE_UNWANTED)) {
                    try {
                        mAsyncTaskListener.responseExtractLogic(response);
                    } catch (Exception E) {
                        if (showDefaultAlerts) {
                            APIUtilities.ShowAlertWithOneButton(TITLE_UNWANTED, MESSAGE_UNWANTED, OK, true, mContext);
                        }
                        mAsyncTaskListener.onFailure(MESSAGE_UNWANTED + ": " + E.toString() + "\n" + E.getStackTrace());
                    }
                } else {
                    if (response.contains("#")) {
                        String[] error = response.split("#");
                        if (showDefaultAlerts) {
                            APIUtilities.ShowAlertWithOneButton(TITLE_UNWANTED, "Error Code: " + error[1] + "\nError: " + error[2], OK, true, mContext);
                        }
                        mAsyncTaskListener.onFailure("Error Code: " + error[1] + "\nError: " + error[2]);
                    } else {
                        if (showDefaultAlerts) {
                            APIUtilities.ShowAlertWithOneButton(TITLE_UNWANTED, MESSAGE_UNWANTED + "\n" + response, OK, true, mContext);
                        }
                        mAsyncTaskListener.onFailure(MESSAGE_UNWANTED + ": " + response);
                    }
                }
            } else {
                if (showDefaultAlerts) {
                    APIUtilities.ShowAlertWithOneButton(TITLE_TIMEOUT, MESSAGE_TIMEOUT, OK, true, mContext);
                }
                mAsyncTaskListener.onFailure(MESSAGE_TIMEOUT);
            }
        } else {
            Toast.makeText(mContext, MESSAGE_NO_INTERNET, Toast.LENGTH_SHORT).show();
            mAsyncTaskListener.onFailure(MESSAGE_NO_INTERNET);
        }
    }
}
